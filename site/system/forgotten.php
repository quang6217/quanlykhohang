<?php
$error_1 = '';
$error_2 = '';
if (isset($_POST['mat_khau_moi'])) {	
	$ma_sp = (int) $_POST['ma_so'];
	if ($ma_sp > 10000) {
		$email = _sql01($_GET['email']);
		$stmt =  $conn->prepare("SELECT * FROM member WHERE email =:email");
		$stmt->execute(array(":email" => $email));
		$member = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($member['code_xac_minh'] == $ma_sp) {
			$id_member = (int) $member['id'];
			$pass_md5 = md5(md5($_POST['pass_new']));
			$sql = "UPDATE member SET code_xac_minh=0, pass='$pass_md5' WHERE id=$id_member";
			$stmt = $conn->prepare($sql);
			$stmt->execute();
			header('Location: /?get=login');
			exit;
		} else {
			$error_1 = '<small style="padding: 50px; color:red">Sai mã xác nhận</small>';
		}
	}
}


if (isset($_GET['email'])) {
	$title = 'Nhập mật khẩu mới';
	require 'site/widget/header_r.php';
	$ma_so = (int) $_GET['ma_so'] > 0 ? (int) $_GET['ma_so'] : '';
?>
	<form class="login100-form validate-form" method="post" action="<?= $_SERVER['REQUEST_URI'] ?>">
		<span class="login100-form-title">
			Điền mật khẩu mới
		</span>
		<?= $error_1 ?>
		<div class="wrap-input100 validate-input" data-validate="Password is required">
			<input class="input100" name="ma_so" value="<?= $ma_so ?>" placeholder="Mã số" required>
			<span class="focus-input100"></span>
			<span class="symbol-input100">
				<i class="fa fa-key" aria-hidden="true"></i>
			</span>
		</div>
		<div class="wrap-input100 validate-input" data-validate="Password is required">
			<input class="input100" name="pass_new" placeholder="Mật khẩu" required>
			<span class="focus-input100"></span>
			<span class="symbol-input100">
				<i class="fa fa-lock" aria-hidden="true"></i>
			</span>
		</div>
		<div class="container-login100-form-btn">
			<button class="login100-form-btn" name="mat_khau_moi">
				Xác nhận
			</button>
		</div>

		<div class="text-center p-t-12">
			<a class="txt2" href="/?sys=login">
				Đăng nhập
			</a>
		</div>

		<div class="text-center p-t-136">
			<a class="txt2" href="/?sys=registration">
				Đăng ký tài khoản
				<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
			</a>
		</div>
	</form>
	</div>
	</div>
	</div>
<?php require 'site/widget/footer_r.php';
	exit;
}

$email_p='';
if (isset($_POST['email'])) {
	$email = _sql01($_POST['email']);
	$stmt =  $conn->prepare("SELECT * FROM member WHERE email =:email");
	$stmt->execute(array(":email" => $email));
	$member = $stmt->fetch(PDO::FETCH_ASSOC);
	$id_member = (int) $member['id'];
	if ($id_member > 0) {
		$random = rand(1000000000, 9999999999);
		$conten = '
Xin chào: <b>' . $member['name'] . '</b>!<br>
Sữ dụng mã số: <b>' . $random . '</b> hoặc <b><a href="https://like88.me/?get=forgotten&email=' . $email . '&ma_so=' . $random . '">nhấn vào đây</a></b> để đặt lại mật khẩu mới cho tài khoản của bạn.<br>
Xin cảm ơn,<br>
<b><i>Đội PHP Developer LIKE88</i></b>';
		$sql = "UPDATE member SET code_xac_minh=$random WHERE id=$id_member";
		$stmt = $conn->prepare($sql);
		$stmt->execute();
		sendMail($email, "Đặt lại mật khẩu LIKE88", $conten);
		header('Location: /?get=forgotten&email=' . $email);
		exit;
	} else {
		$error_2 = '<small style="padding: 50px; color:red">Email không tồn tại</small>';
	}
}
$title = 'Quên mật khẩu';
require 'site/widget/header_r.php';
?>
<form class="login100-form validate-form" method="post" action="<?= $_SERVER['REQUEST_URI'] ?>">
	<span class="login100-form-title">
		Quên mật khẩu
	</span>
<?=$error_2?>
	<div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
		<input class="input100" type="text" name="email" value="<?=$email?>" placeholder="Email">
		<span class="focus-input100"></span>
		<span class="symbol-input100">
			<i class="fa fa-envelope" aria-hidden="true"></i>
		</span>
	</div>

	<div class="container-login100-form-btn">
		<button class="login100-form-btn">
			Lấy lại mật khẩu
		</button>
	</div>

	<div class="text-center p-t-12">
		<a class="txt2" href="/?sys=login">
			Đăng nhập
		</a>
	</div>

	<div class="text-center p-t-136">
		<a class="txt2" href="/?sys=registration">
			Đăng ký tài khoản
			<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
		</a>
	</div>
</form>
</div>
</div>
</div>
<?php require 'site/widget/footer_r.php'; ?>