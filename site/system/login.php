<?php
if ($_GET['exit'] == 'ok') {
	setcookie('id_member', '', $time_php - 100 * 60 * 60);
	setcookie('id_pass', '', $time_php - 100 * 60 * 60);
	session_destroy();
	header('Location: /?get=login');
	exit;
}

//Khai báo các biến ban đầu
$error_1 = '';
$error_2 = '';
$_SESSION['email'] = isset($_SESSION['email']) ? _sql01($_SESSION['email']) : '';

//Hàm xữ lý khi người dùng nhấn POST
if (isset($_POST['login'])) {
	//Lưu lại giá trị post    
	$email = _sql01(($_POST['login_name']));
	$_SESSION['email'] = $email;
	$pass = _sql01(($_POST['pass']));
	$pass_md5 = md5(md5($pass));

	//Lấy thông tin thành viên
	$stmt =  $conn->prepare("SELECT * FROM member WHERE `email`='$email'");
	$stmt->execute(array());
	$referral = $stmt->fetch(PDO::FETCH_ASSOC);
	$id_member = $referral['id'];

	//Nếu đã có lịch sữ sai pass xấu, chặn lại
	if ($referral['pass_error'] > 2 and $referral['time_login'] > $time_php - 300) {
		setcookie('id_member', '', $time_php - 100 * 60 * 60);
		setcookie('id_pass', '', $time_php - 100 * 60 * 60);
		session_destroy();
		$so_giay =  $referral['time_login'] - ($time_php - 300);
?>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.3/jquery.min.js"></script>
		<script>
			$(document).ready(function() {
				var interval;
				$("#processbar").val("0");
				$("#count-down").html("<?= $so_giay ?>");
				clearInterval(interval);
				interval = null;
				var count = <?= $so_giay ?>;

				function loadlink() {
					window.location = "/?get=login";
				}
				interval = setInterval(function() {
					count--;
					if (count === 0) {
						count = <?= $so_giay ?>;
						loadlink()
					}
					$("#count-down").html(count);
					$("#processbar").val(<?= $so_giay ?> - count);
				}, 1000);
			});
		</script>
		<script></script>
		<center>
			<title>Bảo mật đăng nhập</title>
			<div class="content"><span class="has-text-grey is-size-7">Xin thử đăng nhập lại sau: <span id="count-down">30s</span> giây</span><br>
				<progress value="0" max="<?= $so_giay ?>" id="processbar"></progress></div>
			Lý do: Bạn nhập sai mật khẩu nhiều lần!
		</center>
<?php
		exit;
	}

	// Thông báo các lỗi khi gặp phải
	$error_1 = $id_member < 1 ? '<small style="padding: 50px; color:red">Email is incorrect</small>' : '';
	$error_2 = $pass_md5 != $referral['pass'] ? '<small style="padding: 50px; color:red">Password is incorrect</small>' : '';
	$error_2 = $error_1 == '' ? $error_2 : '';

	//Nếu đầy đủ các điều kiện cho phép đăng nhập
	if ($id_member > 0 and $pass_md5 == $referral['pass']) {
		setcookie('id_member', $referral['id'], $time_php + 10 * 60 * 60 * 24);
		setcookie('id_pass', $referral['pass'], $time_php + 10 * 60 * 60 * 24);
		$code_1 = substr($referral['pass'], 0, 10);
		$code_2 = substr($referral['pass'], 10, 20);
		$token_v2 = md5(md5($code_1)) . '' . md5(md5(md5($code_2)));
		setcookie('token', $token_v2, $time_php + 10 * 60 * 60 * 24);
		$_SESSION['id_member'] = $referral['id'];
		$_SESSION['id_pass'] = $referral['pass'];

		//Xóa lịch sữ đăng nhập sai mật khẩu
		$sql = "UPDATE member SET time_login='$time_php', pass_error='0' WHERE id='" . $id_member . "'";
		$stmt = $conn->prepare($sql);
		$stmt->execute();

		$_SESSION['REQUEST_URI'] = isset($_SESSION['REQUEST_URI']) ? $_SESSION['REQUEST_URI'] : '/';
		//Chuyển hướng đến vị trí ban đầu
		header('Location: ' . $_SESSION['REQUEST_URI']);
		exit;
	} else {
		//Ghi lại lỗi login sai mật khẩu
		if ($id_member > 0) {
			$so_pass_error = $referral['pass_error'] + 1;
			$sql = "UPDATE member SET time_login='$time_php', pass_error='$so_pass_error'  WHERE id='" . $id_member . "'";
			$stmt = $conn->prepare($sql);
			$stmt->execute();
		}
	}
}
$title = 'Đăng nhập';
require 'site/widget/header_r.php';
?>

<form class="login100-form validate-form" method="post" action="<?= _sql01($_SERVER['REQUEST_URI']) ?>">
	<span class="login100-form-title">
		Dành cho thành viên
	</span>
	<?= $error_1 ?>
	<div class="wrap-input100 validate-input" data-validate="Valid email is required: ex@abc.xyz">
		<input class="input100" type="text" name="login_name" value="<?= _sql01($_SESSION['email']) ?>" placeholder="Email" required>
		<span class="focus-input100"></span>
		<span class="symbol-input100">
			<i class="fa fa-envelope" aria-hidden="true"></i>
		</span>
	</div>
	<?= $error_2 ?>
	<div class="wrap-input100 validate-input" data-validate="Password is required">
		<input class="input100" type="password" name="pass" placeholder="Mật khẩu" required>
		<span class="focus-input100"></span>
		<span class="symbol-input100">
			<i class="fa fa-lock" aria-hidden="true"></i>
		</span>
	</div>

	<div class="container-login100-form-btn">
		<button class="login100-form-btn" type="submit" name="login">
			Đăng nhập
		</button>
	</div>

	<div class="text-center p-t-12">
		<a class="txt2" href="/?get=registration"> Tạo tài khoản mới</a>
	</div>

	<div class="text-center p-t-136">
		<a class="txt2" href="/?get=forgotten">
			Bạn quên mật khẩu?
			<i class="fa fa-long-arrow-right m-l-5" aria-hidden="true"></i>
		</a>
	</div>
</form>
</div>
</div>
</div>
<?php require 'site/widget/footer_r.php'; ?>