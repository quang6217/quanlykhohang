$stmt =  $conn->prepare("SELECT * FROM vn_san_pham WHERE id =:id");
    $stmt->execute(array(":id" => (int) $_POST['ma_sp']));
    $ma_sp = $stmt->fetch(PDO::FETCH_ASSOC);
    $name_sp = $ma_sp['name'];
    $gia_ban = $ma_sp['gia_ban'];
    $don_vi = $ma_sp['don_vi'];
    $quang_cao_sp = $ma_sp['quang_cao_sp'];
    $hinh_anh = $ma_sp['hinh_anh'];
    $video = $ma_sp['video'];
    $tin_nhan_kh = _sql01(strtolower(convert_vi_to_en($_POST['nd'])));
    $bot_rep = "";
    $stmt1 =  $conn->prepare("SELECT * FROM vn_chat_bot ORDER BY id DESC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $vn_chat_bot) {
        $key_a = explode('|', $vn_chat_bot['key_a']);
        for ($ka = 0; $ka < count($key_a); $ka++) {
            $ndka = trim($key_a[$ka]);
            if ($ndka != "") {
                if (check_string($ndka, $tin_nhan_kh) > 0) {
                    $bot_rep = $vn_chat_bot['key_full'];
                    break;
                }
            }
            if ($bot_rep != "") {
                break;
            }
        }
        if ($bot_rep != "") {
            break;
        }
    }

    if ($bot_rep != "") {
        $bot_rep = str_replace('{name_sp}', $name_sp, $bot_rep);
        $bot_rep = str_replace('{gia_ban}', $gia_ban, $bot_rep);
        $bot_rep = str_replace('{don_vi}', $don_vi, $bot_rep);
        $bot_rep = str_replace('{quang_cao_sp}', $quang_cao_sp, $bot_rep);
        $ar_size = explode('|', $ma_sp['size']);
        $size_sl = count($ar_size);
        $bot_rep = str_replace('{size_sl}', $size_sl, $bot_rep);
        $chi_tiet_size = "";
        $size_chuan_chung = "";
        $dia_chi_shop = $ma_sp['dia_chi_shop'];
        $sdt_shop = $ma_sp['sdt_shop'];
        for ($i = 0; $i < $size_sl; $i++) {
            $stmt =  $conn->prepare("SELECT * FROM vn_size WHERE id =:id AND sex =:sex");
            $stmt->execute(array(":id" => (int) $ar_size[$i], ":sex" => (int) $ma_sp['sex']));
            $mau = $stmt->fetch(PDO::FETCH_ASSOC);
            $ngan_cach = "\n";
            if ($chi_tiet_size == "") {
                $chi_tiet_size = $mau['name_1'];
            } else {
                $chi_tiet_size = $chi_tiet_size . ' - ' . $mau['name_1'];
            }
            $size_chuan_chung = $size_chuan_chung . $ngan_cach . $mau['name'] . ' phù hợp Cao: ' . $mau['chieu_cao'] . ' - Nặng: ' . $mau['can_nang'] . ' - Eo: ' . $mau['vong_eo'] . ' - Ngực: ' . $mau['vong_nguc'] . '';
        }
        $bot_rep = str_replace('{chi_tiet_size}', $chi_tiet_size, $bot_rep);
        $bot_rep = str_replace("{size_chuan_chung}", $size_chuan_chung . "\n", $bot_rep);
        $ar_mau = explode('|', $ma_sp['mau_sac']);
        $chi_tiet_ms = "";
        for ($i = 0; $i < count($ar_mau); $i++) {
            $stmt =  $conn->prepare("SELECT * FROM vn_mau_sac WHERE id =:id");
            $stmt->execute(array(":id" => (int) $ar_mau[$i]));
            $mau = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($chi_tiet_ms == "") {
                $chi_tiet_ms = $mau['name'];
            } else {
                $chi_tiet_ms = $chi_tiet_ms . ' - ' . $mau['name'];
            }
        }
        $chat_lieu_sp = $ma_sp['chat_lieu_sp'];
        $shop_xung_ho = $ma_sp['shop_xung_ho'];
        $khach_xung_ho = $ma_sp['khach_xung_ho'];
        $bot_rep = str_replace("{chi_tiet_ms}", $chi_tiet_ms . " ", $bot_rep);
        $bot_rep = str_replace("{dia_chi_shop}", $dia_chi_shop . " ", $bot_rep);
        $bot_rep = str_replace("{sdt_shop}", $sdt_shop, $bot_rep);
        $bot_rep = str_replace("{chat_lieu_sp}", $chat_lieu_sp, $bot_rep);
        $bot_rep = str_replace("{shop_xung_ho}", $shop_xung_ho, $bot_rep);
        $bot_rep = str_replace("{khach_xung_ho}", $khach_xung_ho, $bot_rep);
        $bot_rep = "id: " . $vn_chat_bot["id"] . "\r" . $bot_rep;
        echo '<textarea class="form-control" rows="10" placeholder="Bót trả lời" disabled>' . $bot_rep . '</textarea>';
    } else {
        $bot_rep = "Bót ko hiểu\nVui lòng hỏi câu dễ hơn 1 chút";
        echo '<textarea class="form-control" rows="10" placeholder="Bót trả lời" disabled>' . $bot_rep . '</textarea>';
    }
    exit;