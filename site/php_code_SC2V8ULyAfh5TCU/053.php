<?php
if ($id_level < 8) {
    echo "Bạn không có quyền truy cập vào chức năng này!!!";
    exit;
}
if(isset($_POST['edit_al_nguon_kh'])){
    $uid = (int)$_POST['edit_al_nguon_kh'];
    $name = _sql01($_POST['name_team']);
    $sql = "UPDATE admin_nguon_khach_hang SET name='$name' WHERE id='" . $uid . "'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    exit;
}

if (isset($_POST['type_toggle'])) {
    if ($id_level < 7) {
        exit;
    }
    $uid = (int)$_POST['uid'];
    $type = (int)$_POST['type_toggle'];
    $sql = "UPDATE admin_nguon_khach_hang SET an='$type' WHERE id='" . $uid . "'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
    exit;
}

if (isset($_POST['xacnhan_tao'])) {
    $array_3 = explode('<br />', nl2br(trim($_POST['name_new'])));
    foreach ($array_3 as $na) {
        $name = _sql01($na);
        $stmt =  $conn->prepare("SELECT * FROM sale_sanpham_01 WHERE name='$name'");
        $stmt->execute();
        $ia_na = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($ia_na['id'] > 0) {
            $thong_bao_kq = '' . $thong_bao_kq . '<li>  Tên: <font color=red>' . $name . '</font> - <font color=red>Trùng tên</font> </li>';
        } else {
            $sql = "INSERT INTO `admin_nguon_khach_hang`(`name`, `an`) VALUES ('$name','0')";
            $conn->exec($sql);
            $thong_bao_kq = '' . $thong_bao_kq . '<li>  Lý do: <font color=blue>' . $name . '</font> - <font color=blue>Thêm thành công</font> </li>';
        }
    }
    $thong_bao_chuan = '<div class="row"><div class="col-12"><div class="card"><div class="card-header"><h5 class="card-title">Kết quả thêm lý do</h5></div><ol>' . $thong_bao_kq . '</ol></div></div></div></div>';
    header('Location: /al_nguon_kh');
    exit;
}

if (isset($_POST['list_member_team'])) {
    $id_team =  (int)$_POST['list_member_team'];
    $stmt1 =  $conn->prepare("SELECT * FROM member WHERE team=$id_team AND id!=9 ORDER BY id DESC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    echo '<select class="custom-select flex-grow-1" id="id_mem_ne_' . $id_team . '" onblur="update_show_name(' . $id_team . ')">';
    foreach ($list_code as $show) {
        echo '<option value="' . $show['id'] . '">' . $show['name'] . '</option>';
    }
    echo '</select>';
    exit;
}
$title = 'Thêm nguồn hàng';
require 'site/widget/header.php'; ?>
<main class="content">
    <div class="container-fluid p-0">
        <?= $thong_bao_chuan ?>
        <div class="row">
            <div class="ml-3" id="headingTwo" style="display: inline-flex;">
                <h3 class="mt-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        Thêm nguồn khách mới
                    </a>
                </h3>
            </div>
            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapseTwo" class="card collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-row">
                                        <div class="form-group col-md-12">
                                            <textarea type="text" class="form-control" rows="5" value="" name="name_new" placeholder="Mổi dòng là 1 TEAM mới"></textarea>
                                        </div>
                                        <div class="form-group col-md-12">
                                            <button type="submit" name="xacnhan_tao" class="btn btn-block btn-flat btn_xam text_black">Xác nhận</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 mt-2">
                <div class="card">
                    <div class="card-header alert-info">
                        <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size: px;color:white;">
                            Các nguồn khách của bạn
                        </div>
                    </div>
                    <div class="">
                        <div class="table-responsive flex-row flex-nowrap">
                            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="text-align:center; width:5%;">#</th>
                                        <th style="text-align:left;">Lý do</th>
                                        <th style="text-align:center;">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sqlAll = "SELECT COUNT(*) FROM `admin_nguon_khach_hang`";
                                    $stmt5 = $conn->query($sqlAll);
                                    $total_records  = $stmt5->fetchColumn();
                                    $limit = $member['limit_page'] > 0 ? $member['limit_page'] : 20;
                                    $total_page = ceil($total_records / $limit);
                                    $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 0;
                                    $_GET['page'] = $_GET['page'] > 0 ? $_GET['page'] : 0;
                                    if ($total_page > 0) {
                                        $total_page_max = $total_page - 1;
                                    } else {
                                        $total_page_max = $total_page;
                                    }
                                    $_GET['page'] = $total_page_max < $_GET['page'] ? $total_page_max : $_GET['page'];
                                    $start_page = $_GET['page'] * $limit;
                                    $num_1 = 0;
                                    $stmt1 =  $conn->prepare("SELECT * FROM admin_nguon_khach_hang ORDER BY id ASC LIMIT $start_page, $limit");
                                    $stmt1->execute(array());
                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                    $num = 0;
                                    foreach ($list_code as $show_fp) {
                                        $num_1 = $num_1 + 1;
                                        $num = $num_1 + $_GET['page'] * $limit;
                                        $a_on = 'on';
                                        if ($show_fp['an'] == 44) {
                                            $a_on = 'off';
                                        }
                                        echo '<tr>
                                            <td style="text-align:center;">' . $num . '</td>
                                            <td style="font-family: center" id="show_name_' . $show_fp['id'] . '">' . substr($show_fp['name'],  0, 100)  . '</td>                                             
                                            <td style="text-align:center;" id="quyen_edit_' . $show_fp['id'] . '">
                                            <a href="javascript:void(0)" onclick="edit_team(' . $show_fp['id'] . ')"><i class="align-middle mr-2 fas fa-fw fa-pen"></i></a>
                                            <a href="javascript:void(0)" onclick="hidden_show_name(' . $show_fp['id'] . ')"><i class="align-middle mr-2 fas fa-fw fa-toggle-'.$a_on.'"></i></a>
                                            </td>
                                            </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php
                $i = 1;
                for ($i; $i <= $num; $i++) {
                    echo $chuoi_popup_edit[$i];
                }
                load_page(_sql01($_SERVER['REQUEST_URI']), $total_page, $limit, $total_records, $total_page_max);
                load_dialog($total_page_max, $member['id']); ?>
            </div>
        </div>
    </div>
    <script>
        function edit_team(uid) {
            name = document.getElementById("show_name_" + uid).innerHTML;
            var abc = name.split('<input class=');
            if (abc.length > 1) {
                return;
            }
            document.getElementById("show_name_" + uid).innerHTML = "<input class=\"form-control\" onblur=\"update_show_name(" + uid + ")\" id=\"show_name1_" + uid + "\" value=\"" + name + "\">";          
        }

        function update_show_name(uid) {
            name_team = document.getElementById("show_name1_" + uid).value;            
            $.post("al_nguon_kh", {
                edit_al_nguon_kh: uid,
                name_team: name_team
            }, function(data, status) {
                if (status == "success") {                    
                    document.getElementById("show_name_" + uid).innerHTML = name_team;
                }
            });
        }
        function hidden_show_name(uid) {
            a_string = document.getElementById("quyen_edit_" + uid).innerHTML;
            var abc = a_string.split('fa-toggle-on');
            if (abc.length > 1) {
                type_toggle = 44;
                a_string = a_string.replace("fa-toggle-on", "fa-toggle-off");
            } else {
                type_toggle = 0;
                a_string = a_string.replace("fa-toggle-off", "fa-toggle-on");
            }
            document.getElementById("quyen_edit_" + uid).innerHTML = a_string;
            $.post("al_nguon_kh", {
                type_toggle: type_toggle,
                uid: uid
            }, function(data, status) {});
        }
    </script>