<?php

if ($id_level == 5) {
    header('Location: /');
    exit;
}
function _sql_date($t)
{
    return preg_replace('/[^0-9-]/', '', $t);
}
// function get_money_by_id($id)
// {       global $conn;
//         $stmt =  $conn->prepare("SELECT * FROM gia_nhap_xuat");
//         $stmt->execute(array());
//         $data = $stmt->fetchALL(PDO::FETCH_ASSOC);
//         foreach($data as $d)
//         {
//             if($id == $d["id"] )
//                 {
//                     return $d;
//                 }
//         }
//         return NULL;
// }
if (isset($_GET['search'])) {
    if ($_GET['search'] == 0) {
        $_SESSION['tim_kiem_list_hang'] = '1=1';
        $_SESSION['data_tim_kiem'] = '';
        header('Location: /tk_don_ngay');
        exit;
    }
}
$data_nguon_khach = '1=1';
if (isset($_POST['button_xn_tim_kiem'])) {
    $in_member = (int)$_POST['in_member'];
    $data_in_member = '1=1';
    if ($in_member > 0) {
        $data_in_member = '(`mem`=' . $in_member . ')';
    }
    $data_in_team = '1=1';
    $in_team = (int)$_POST['in_team'];
    if ($in_team > 0) {
        $data_in_team = 'team=' . $in_team;
    }
    $data_nguon_khach = '1=1';
    $id_nguon_khach = (int)$_POST['id_nguon_khach'];
    if ($id_nguon_khach > 0) {
        $data_nguon_khach = 'nguonKH=' . $id_nguon_khach;
    }
    $data_status_ly_do = 'type_dh=0';
    $id_status_ly_do = (int)$_POST['id_status_ly_do'];
    if ($id_status_ly_do > 0) {
        if ($id_status_ly_do < 1000) {
            $data_status_ly_do = 'type_dh=' . $id_status_ly_do;
        } else {
            $id_stt = $id_status_ly_do - 1000;
            $data_status_ly_do = 'trang_thai=' . $id_stt;
        }
    }
    $a_date_bd = '1=1';
    if ($_POST['date_bd'] != '') {
        $i_date_bd = _sql_date($_POST['date_bd']);
        $date_bd = _sql_num($_POST['date_bd']);
        $a_date_bd = '(`time_num`>=' . (int)$date_bd . ')';
    }
    $a_date_kt = '1=1';
    if ($_POST['date_kt'] != '') {
        $i_date_kt = _sql_date($_POST['date_kt']);
        $date_kt = _sql_num($_POST['date_kt']);
        $a_date_kt = '(`time_num`<=' . (int)$date_kt . ')';
    }
    $_SESSION['data_tim_kiem'] = $in_team . '|' . $id_nguon_khach . '|' . $id_status_ly_do . '|' . $in_member . '|' . $i_date_bd . '|' . $i_date_kt;
    $_SESSION['tim_kiem_list_hang'] = $data_in_team . ' AND ' . $data_nguon_khach . ' AND ' . $data_status_ly_do . ' AND ' . $data_in_member;
    $_SESSION['data_in_team'] = $data_in_team;
    $_SESSION['data_in_member'] = $data_in_member;
    $_SESSION['data_nguon_khach'] = $data_nguon_khach;
}
if (!isset($_SESSION['data_in_member'])) {
    $_SESSION['data_in_member'] = '1=1';
}
if (!isset($_SESSION['data_in_team'])) {
    $_SESSION['data_in_team'] = '1=1';
}
if (!isset($_SESSION['tim_kiem_list_hang'])) {
    $_SESSION['tim_kiem_list_hang'] = '1=1';
    $class_show = '';
} else {
    if ($_SESSION['tim_kiem_list_hang'] != '1=1') {
        $arr_t = explode('|', $_SESSION['data_tim_kiem']);
        $class_show = 'show';
    }
}
if ($i_date_bd < 1) {
    $i_date_bd = date('Y-m', $time_php) . '-01';
}
if ($i_date_kt < 1) {
    $i_date_kt = date('Y-m-d', $time_php);
}
$timkiem = $_SESSION['tim_kiem_list_hang'];
$list_tk = 'active';
$list_tk_1 = 'show';
if (isset($_POST['check_don_ngay_sum'])) {
    $timkiem = str_replace('AND type_dh=0 ','',$timkiem);
    $ngay_bd = _sql_num(explode('|', $_POST['check_don_ngay_sum'])[0]);
    $ngay_kt = _sql_num(explode('|', $_POST['check_don_ngay_sum'])[1]);
    $don_hang = (int)$_POST['don_hang'];
    $thuc_nhan = (int)$_POST['thuc_nhan'];
    $sqlAll = "SELECT COUNT(`id`) FROM `sale_sanpham_03`WHERE `time_num`>=$ngay_bd AND `time_num`<=$ngay_kt AND $timkiem";
    $stmt = $conn->query($sqlAll);
    $num_rows = $stmt->fetchColumn();
    echo 'Ngày: ' . date_to_str($ngay_bd) . ' -->' . date_to_str($ngay_kt) . '|||';
    echo '<div class="table-responsive flex-row flex-nowrap"><table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%"><thead> <tr> <th style="text-align:center;">Tên mục</th><th colspan="2" style="text-align:center;">Thống kê</th></tr></thead><tbody>';
    echo '<tr><td style="text-align:center;">Tỷ lệ chốt</td>
              <td style="text-align:center;" colspan="2">  ' . number_format($don_hang / $num_rows * 100) . '% <sup style="color:blue";>(' . $don_hang . '/' . $num_rows . ')</sup></td></tr>';
    echo '<tr><td style="text-align:center;">Trung bình đơn</td>
              <td style="text-align:center;" colspan="2">  ' . number_format($thuc_nhan / $don_hang) . 'đ</td></tr>';
    echo '</table>';
    if ($id_level > 7) {
        $timkiem_team = $_SESSION['data_in_team'];
        $timkiem_mem = $_SESSION['data_in_member'];
        echo '<br><table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%"><thead><tr> <th style="text-align:center;">Mã kèm</th><th style="text-align:center;">Sl * giá bán</th><th style="text-align:center;">Vốn nhập</th><th style="text-align:center;">Tiền bán</th></tr>';
        $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`sl` ) as `total_sl` FROM `sale_sanpham_04` AS  `a` WHERE `time_num`>=$ngay_bd AND `time_num`<=$ngay_kt AND  $timkiem_team AND $timkiem_mem GROUP BY `sp_02` ORDER BY `total_sl` ASC");
        $stmt1->execute(array());
        $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
        foreach ($list_code as $show_fp) {
            $stmt =  $conn->prepare("SELECT * FROM sale_sanpham_02 WHERE id =:id");
            $stmt->execute(array(":id" => $show_fp['sp_02']));
            $von = $stmt->fetch(PDO::FETCH_ASSOC);
            $tong_tien = $tong_tien + $show_fp['total_sl'] * $von['gia_nhap'];
            echo '<tr>
                <td style="text-align:center;">  ' . $von['ma_kem'] . '</td>
                <td style="text-align:center;">  ' . number_format($show_fp['total_sl']) . ' * ' . number_format($von['gia_nhap']) . '</td>
                <td style="text-align:center;">  ' . number_format($show_fp['total_sl'] * $von['gia_nhap']) . '</td>
                <td style="text-align:center;">  ' . number_format($show_fp['total_sl'] * $von['gia_tien']) . '</td></tr>';
        }
        echo '<tr>
        <td style="text-align:center; color:red;" colspan="2">Tổng tiền vốn</td>
        <td style="text-align:center; color:red;" colspan="2">  ' . number_format($tong_tien) . '</td></tr>';
    }
    exit;
}
if (isset($_POST['check_don_ngay'])) {
    $timkiem = str_replace('AND type_dh=0 ','',$timkiem);
    echo 'Ngày: ' . date_to_str($_POST['check_don_ngay']) . '|||';
    echo '<div class="table-responsive flex-row flex-nowrap"><table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%"><thead> <tr> <th style="text-align:center;">Tên mục</th><th colspan="2" style="text-align:center;">Thống kê</th></tr></thead><tbody>';
    $time_ngay = (int)$_POST['check_don_ngay'];
    $don_hang = (int)$_POST['don_hang'];
    $thuc_nhan = (int)$_POST['thuc_nhan'];
    $sqlAll = "SELECT COUNT(`id`) FROM `sale_sanpham_03`WHERE `time_num`=$time_ngay AND $timkiem AND `trang_thai` != 44";
    $stmt = $conn->query($sqlAll);
    $num_rows = $stmt->fetchColumn();
    // echo $sqlAll;
    echo '<tr><td style="text-align:center;">Tỷ lệ chốt</td>
              <td style="text-align:center;" colspan="2">  ' . number_format($don_hang / $num_rows * 100) . '% <sup style="color:blue";>(' . $don_hang . '/' . $num_rows . ')</sup></td></tr>';
    echo '<tr><td style="text-align:center;">Trung bình đơn</td>
              <td style="text-align:center;" colspan="2">  ' . number_format($thuc_nhan / $don_hang) . 'đ</td></tr>';
    echo '</table>';
    if ($id_level > 7) {
        $timkiem_team = $_SESSION['data_in_team'];
        $timkiem_mem = $_SESSION['data_in_member'];
        $data_nguon_khach = $_SESSION['data_nguon_khach'];
        if(isset($_SESSION['data_nguon_khach']))
        {
            $data_nguon_khach = $_SESSION['data_nguon_khach'];
        }else
        {
            $data_nguon_khach = '1=1';
        }
        echo '<br><table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%"><thead><tr> <th style="text-align:center;">Mã kèm</th><th style="text-align:center;">Sl * giá bán</th><th style="text-align:center;">Vốn nhập</th><th style="text-align:center;">Tiền bán</th></tr>';
        $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`sl` ) as `total_sl` FROM `sale_sanpham_04` AS  `a` WHERE `time_num`=$time_ngay AND $timkiem_team AND $timkiem_mem AND $data_nguon_khach GROUP BY `sp_02` ORDER BY `total_sl` ASC");
        $stmt1->execute(array());
        $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
        // echo "SELECT * , sum(`a`.`sl` ) as `total_sl` FROM `sale_sanpham_04` AS  `a` WHERE `time_num`=$time_ngay AND $timkiem_team AND $timkiem_mem AND $data_nguon_khach GROUP BY `sp_02` ORDER BY `total_sl` ASC";
        foreach ($list_code as $show_fp) {
            $stmt =  $conn->prepare("SELECT * FROM sale_sanpham_02 WHERE id =:id");
            $stmt->execute(array(":id" => $show_fp['sp_02']));
            $von = $stmt->fetch(PDO::FETCH_ASSOC);
            $trang_thai = "";
            $so_lan =  get_data_by_id_gia_nhap_xuat($show_fp["id_gia_tien"])["so_lan"];
            $gia_xuat_nhap = get_id_so_lan_max($von["id"]);
            $so_lan_new = $gia_xuat_nhap["so_lan"];
            $gia_nhap = get_data_by_id_gia_nhap_xuat($show_fp["id_gia_tien"])["gia_nhap"];
            $gia_xuat = get_data_by_id_gia_nhap_xuat($show_fp["id_gia_tien"])["gia_xuat"];
            $tong_tien = $tong_tien + $show_fp['total_sl'] * $gia_nhap;
            if($so_lan != $so_lan_new)
            {
                $trang_thai = "(Giá cũ)";
            }
            echo '<tr>
                <td style="text-align:center;">  ' . $von['ma_kem'] . '</td>
                <td style="text-align:center;">  ' .number_format($show_fp['total_sl']) . ' * ' . number_format($gia_nhap) .$trang_thai. '</td>
                <td style="text-align:center;">  ' . number_format($show_fp['total_sl'] * $gia_nhap) . '</td>
                <td style="text-align:center;">  ' . number_format($show_fp['total_sl'] * $gia_xuat) . '</td></tr>';
        }
        echo '<tr>
        <td style="text-align:center; color:red;" colspan="2">Tổng tiền vốn</td>
        <td style="text-align:center; color:red;" colspan="2">  ' . number_format($tong_tien) . '</td></tr>';
    }
    exit;
}


$title = "Đơn hàng đã lên theo ngày";
require 'site/widget/header.php'; ?>
<div class="content">
    <div class="row">
        <div class="col-12 mb-3" id="accordion">
            <div id="collapse_TaoDonHang" class="card collapse <?= $class_show ?>" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card" style="margin-bottom: -0.5rem;">
                    <div class="card-body">
                        <form method="post">
                            <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12">
                                <div class="row">
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Nguồn KH</span>
                                            </div>
                                            <select class="custom-select flex-grow-1" name="id_nguon_khach">
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM admin_nguon_khach_hang WHERE an=0 ORDER BY id ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                echo '<option value="0">...</option>';
                                                foreach ($list_code as $show) {
                                                    $selected_ck_2 = $arr_t[1] == $show['id'] ? 'selected' : '';
                                                    echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Trạng thái</span>
                                            </div>
                                            <select class="custom-select flex-grow-1" name="id_status_ly_do">
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM admin_ly_do_chot_xit WHERE an=0 ORDER BY id ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                echo '<option value="0">...</option>';
                                                foreach ($list_code as $show) {
                                                    $selected_ck_2 = $arr_t[2] == $show['id'] ? 'selected' : '';
                                                    echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12">
                                <div class="row">
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Team</span>
                                            </div>
                                            <select class="custom-select flex-grow-1" name="in_team" id="check_team_on" onchange="check_member_team()">
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM danh_sach_team ORDER BY id ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                echo '<option value="0">...</option>';
                                                foreach ($list_code as $show) {
                                                    $selected_ck_2 = $arr_t[0] == $show['id'] ? 'selected' : '';
                                                    echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name_team'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Member</span>
                                            </div>
                                            <select class="custom-select flex-grow-1" name="in_member" id="member_js_sha">
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM member WHERE id!=9 ORDER BY id  ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                echo '<option value="0">...</option>';
                                                foreach ($list_code as $show) {
                                                    $selected_ck_2 = $arr_t[3] == $show['id'] ? 'selected' : '';
                                                    echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Ngày bắt đầu</span>
                                            </div>
                                            <input type="date" name="date_bd" value="<?= $arr_t[4] ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Kết thúc</span>
                                            </div>
                                            <input type="date" name="date_kt" value="<?= $arr_t[5] ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-xs-right">
                                <button type="submit" name="button_xn_tim_kiem" id="button_xn_tim_kiem" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card mb-3">
                <div class="card-header alert-info">
                    <h5 class="card-title mb-0  text-white fs_18 d-flex justify-content-between" style="font-weight: bold;">
                        <div>Đơn hàng đã lên theo ngày</div><i class="align-middle mr-2 fas fa-fw fa-search" data-toggle="collapse" data-target="#collapse_TaoDonHang"></i></small>
                    </h5>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center;">Ngày</th>
                            <th style="text-align:center;">Đơn</th>
                            <th style="text-align:center;">Tổng tiền</th>
                            <th style="text-align:center;">VAT</th>
                            <th style="text-align:center;">Giảm giá</th>
                            <th style="text-align:center;">Ship</th>
                            <th style="text-align:center;">Thực nhận</th>

                        </tr>
                    </thead>
                    <tbody id="top_online">
                        <?php
                        $y =  dem_so_ngay($i_date_bd, $i_date_kt);
                        $i1 =  dem_so_ngay($i_date_kt, date('Y-m-d', $time_php)) - 1;
                        $s_don = 0;
                        $s_tien = 0;
                        $s_vat = 0;
                        $s_giam = 0;
                        $s_ship = 0;
                        $s_nhan = 0;
                        for ($i = 0; $i < $y; $i++) {
                            $time_tru = $i * 24 * 60 * 60 + $i1 * 24 * 60 * 60;
                            $a_time = date('Ymd', $time_php - $time_tru);
                            $sqlAll = "SELECT sum(`tong_tien`) as tong_tien, sum(`thuc_nhan`) as thuc_nhan, sum(`vat`) as vat, sum(`ship`) as ship, sum(`giam_gia`) as giam_gia, count(`id`) as don_hang 
                            FROM `sale_sanpham_03` WHERE 
                            time_num = $a_time AND 
                            trang_thai!=44 AND 
                            (type_dh<1 OR type_dh>8) AND
                             $timkiem";
                            $stmt = $conn->query($sqlAll);
                            $a1 = $stmt->fetch();
                            $s_don = $s_don + $a1['don_hang'];
                            $s_tien = $s_tien + $a1['tong_tien'];
                            $s_vat = $s_vat + $a1['vat'];
                            $s_giam = $s_giam + $a1['giam_gia'];
                            $s_ship = $s_ship + $a1['ship'];
                            $s_nhan = $s_nhan + $a1['thuc_nhan'];
                            echo '<tr>
                          <td style="text-align:center;">' . date('d-m-Y', $time_php - $time_tru) . '</td>
                          <td style="text-align:center;">' . number_format($a1['don_hang'], 0) . '</td>
                          <td style="text-align:center;">' . number_format($a1['tong_tien']) . '</td>
                          <td style="text-align:center;">' . number_format($a1['vat']) . '</td>
                          <td style="text-align:center;">' . number_format($a1['giam_gia']) . '</td>
                          <td style="text-align:center;">' . number_format($a1['ship']) . '</td>
                          <td style="text-align:center;" onclick="chi_tiet(' . date('Ymd', $time_php - $time_tru) . ',' . $a1['don_hang'] . ',' . $a1['thuc_nhan'] . ',0,0)">' . number_format($a1['thuc_nhan']) . '</td>
                          </tr>';
                        }
                        echo '<tr>
                          <td style="text-align:center; color:red">Tổng</td>
                          <td style="text-align:center; color:red">' . number_format($s_don) . '</td>
                          <td style="text-align:center; color:red">' . number_format($s_tien) . '</td>
                          <td style="text-align:center; color:red">' . number_format($s_vat) . '</td>
                          <td style="text-align:center; color:red">' . number_format($s_giam) . '</td>
                          <td style="text-align:center; color:red">' . number_format($s_ship) . '</td>
                          <td style="text-align:center; color:red" onclick="chi_tiet(9999,' . $s_don . ',' . $s_nhan . ',\'' . $i_date_bd . '\',\'' . $i_date_kt . '\')">' . number_format($s_nhan) . '</td>
                          </tr>';
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="popup_ne" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg_xanh_duong_nhat">
                <h2 style="text-align:center;" id="popup_title"></h2>
                <button type="button" class="btn btn-outline-danger" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" id="popup_noidung"></div>
        </div>
    </div>
</div>
<script>
    function chi_tiet(ngay, don_hang, thuc_nhan, ngay_bd_a, ngay_kt_a) {
        if (ngay == 9999) {
            $.post("tk_don_ngay", {
                check_don_ngay_sum: ngay_bd_a + "|" + ngay_kt_a,
                don_hang: don_hang,
                thuc_nhan: thuc_nhan
            }, function(data, status) {
                var ar = data.split('|||');
                document.getElementById("popup_title").innerHTML = ar[0];
                document.getElementById("popup_noidung").innerHTML = ar[1];
                $('#popup_ne').modal('show');
            });
        } else {
            $.post("tk_don_ngay", {
                check_don_ngay: ngay,
                don_hang: don_hang,
                thuc_nhan: thuc_nhan
            }, function(data, status) {
                var ar = data.split('|||');
                document.getElementById("popup_title").innerHTML = ar[0];
                document.getElementById("popup_noidung").innerHTML = ar[1];
                $('#popup_ne').modal('show');
            });
        }
    }

    function check_member_team() {
        abc = document.getElementById("check_team_on").value;
        $.post("list_hang", {
            check_team_on: abc
        }, function(data) {
            document.getElementById("member_js_sha").innerHTML = data;
        });
    }
</script>