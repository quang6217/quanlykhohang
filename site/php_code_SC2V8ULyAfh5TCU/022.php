<?php
if (isset($_POST['post_team'])) {
    $sql_team = '1=1';
    $name_team = 'all';
    $post_team = (int)$_POST['post_team'];
    if ($post_team > 0) {
        $sql_team = 'team=' . $post_team;
    }
    $sqlAll = "SELECT COUNT(`id`) FROM `sale_sanpham_03` WHERE `ma_so_xuat`=0 AND trang_thai=0 AND (type_dh<1 OR type_dh>8) AND $sql_team ORDER BY `id` ASC";
    $stmt = $conn->query($sqlAll);
    $num_rows = $stmt->fetchColumn();
    if ($post_team > 0) {
        $name_team = sql_data($post_team, 'name_team', 'danh_sach_team');
    }
    echo $num_rows . '|' . $name_team;
    exit;
}

if (isset($_GET['ghtk'])) {
    $code = (int) $_GET['ghtk'];
    if ($code < 1000000000) {
        header('Location: /');
        exit;
    }

    $time_giam = $time_php - 24 * 60 * 60;
    $stmt =  $conn->prepare("SELECT * FROM sale_sanpham_03_xuat WHERE code_xuat =:code_xuat");
    $stmt->execute(array(":code_xuat" => $code));
    $xuat_ne = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($xuat_ne['time_xuat'] < $time_giam and $id_level < 6) {
        echo 'error';
        exit;
    }
    if (isset($_GET['tb'])) {
        echo '<style>table,th,td{border:1px solid #000;border-collapse:collapse}th,td{padding:5px}th{text-align:center}</style>';
    }
    echo '<title>Xuất list đơn hàng</title>';
    echo '<table style="width:100%">';
    echo '<tr>
        <th>Mã NB</th>
        <th>SĐT</th>
        <th>Tên KH</th>
        <th>Địa chỉ chi tiết</th>
        <th>Sản phẩm</th>
        <th>Trọng lượng</th>
        <th>Ghi chú</th>
        <th>COD</th>
        </tr>';

    $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_03 WHERE ma_so_xuat=$code AND trang_thai=0 AND (type_dh<1 OR type_dh>8) ORDER BY id DESC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $num = 0;
    $sql = "DELETE FROM sale_sanpham_03_xuat_sum WHERE code_xuat=$code";
    $conn->exec($sql);
    
    foreach ($list_code as $show) {
        $so_luong_hien_thoi1 = 0;
        $trongluong_dh = '';
        $id_dh = $show['id'];
        $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_04 WHERE sp_03=$id_dh ORDER BY id ASC");
        $stmt1->execute();
        $list_code1 = $stmt1->fetchALL(PDO::FETCH_ASSOC);
        $ten_hang = "";
        $sql111 = "SELECT * FROM sale_sanpham_04 WHERE sp_03=$id_dh ORDER BY id ASC";
        foreach ($list_code1 as $show_fp) {
            if ($ten_hang == "") {
                $ten_hang = number_format($show_fp['sl']) . ' ' . sql_sanpham_02($show_fp['sp_02'], 'ma_kem');
            } else {
                $ten_hang = $ten_hang . ' + ' . number_format($show_fp['sl']) . ' ' . sql_sanpham_02($show_fp['sp_02'], 'ma_kem');
            }
            $id_dh_mk = (int)$show_fp['sp_02'];
            $so_luong_mk = (int)$show_fp['sl'];
            $stmt_mk =  $conn->prepare("SELECT * FROM sale_sanpham_03_xuat_sum WHERE code_xuat=$code AND ma_kem=$id_dh_mk");
            $stmt_mk->execute(array(":id" => $vbd_id));
            $sum_mk = $stmt_mk->fetch(PDO::FETCH_ASSOC);
            $id_mk_log = (int)$sum_mk['id'];
            $so_luong_hien_thoi = (int) $sum_mk["so_luong"];
            // echo $so_luong_mk." ".$id_mk_log ." ";
            if ($id_mk_log > 0) {
                $sql = "UPDATE sale_sanpham_03_xuat_sum SET so_luong=$so_luong_hien_thoi+$so_luong_mk WHERE id=$id_mk_log";
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            } else {
                try {
                    $stmt = $conn->prepare('INSERT INTO sale_sanpham_03_xuat_sum (`so_luong`, `ma_kem`, `code_xuat`) values (:so_luong, :ma_kem, :code_xuat)');
                    $data = array('so_luong' => $so_luong_mk, 'ma_kem' => $id_dh_mk, 'code_xuat' => $code);
                    $stmt->execute($data);
                } catch (Exception $e) {
                    // echo $so_luong_mk . ' - ' . $id_dh_mk . ' - ' . $code . '-----------------<br>';
                }
            }
        }
        // echo "<br>";

        if ($ten_hang != '') {
            echo '
            <tr>
            <td>' . $show['id'] . '</td>
            <td>0' . $show['sdt'] . '</td>
            <td>' . $show['nameKH'] . '</td>
            <td>' . $show['dia_chi'] . ', ' . info_vn_xa_v2($show['id_xa']) . '</td>
            <td>' . $ten_hang . '</td>
            <td>' . $trongluong_dh . '</td> 
            <td>' . $show['ghi_chu'] . '</td>
            <td style="text-align:center">' . number_format($show['thuc_nhan']) . '</td>
            </tr>
            ';
        }
    }
    echo '</table>';
    echo '<br><br><table style="width:100%"><tr>
        <th>#</th>
        <th>Số lượng</th>
        <th>Tên mã kèm</th>
        </tr>';

    $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_03_xuat_sum WHERE code_xuat=$code ORDER BY so_luong ASC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $num = 0;
    foreach ($list_code as $show) {
        $num++;
        echo '
        <tr>
        <td style="text-align:center">' . $num . '</td>
        <td style="text-align:center">' . number_format($show['so_luong']) . '</td>
        <td style="text-align:center">' . sql_sanpham_02($show['ma_kem'], 'ma_kem') . '</td>
        </tr>
        ';
    }
    exit;
}
if ($id_level < 5) {
    header('Location: /');
    exit;
}
$error = "";
if (isset($_POST['xac_nhan'])) {
    $sql_team = '1=1';
    $post_team = (int)$_POST['in_team'];
    if ($post_team > 0) {
        $sql_team = 'team=' . $post_team;
    }
    $sqlAll = "SELECT COUNT(`id`) FROM `sale_sanpham_03` WHERE `ma_so_xuat`=0 AND trang_thai=0 AND (type_dh<1 OR type_dh>8) AND $sql_team ORDER BY `id` ASC";
    $stmt = $conn->query($sqlAll);
    $num_rows = $stmt->fetchColumn();
    $so_luong = (int) $_POST['so_luong'];
    $ghi_chu = _sql($_POST['ghi_chu']);
    $code_rand = rand(1000000000000, 999999999999999);
    $so_luong = $so_luong > (int) $num_rows ? (int) $num_rows : $so_luong;
    if ($so_luong > 0) {
        try {
            //Ghi lịch sữ xuất
            $stmt = $conn->prepare('INSERT INTO sale_sanpham_03_xuat (`mem`, `so_luong`, `code_xuat`, `ghi_chu`, `time_xuat`) values (:mem, :so_luong, :code_xuat, :ghi_chu, :time_xuat)');
            $data = array('mem' => $id_member, 'so_luong' => $so_luong, 'code_xuat' => $code_rand, 'ghi_chu' => $ghi_chu, 'time_xuat' => $time_php);
            $stmt->execute($data);
            //Lưu lại đã xuất
            $sql = "UPDATE sale_sanpham_03 SET ma_so_xuat='$code_rand' WHERE ma_so_xuat= 0  AND trang_thai=0 AND (type_dh<1 OR type_dh>8) AND $sql_team ORDER BY `id` ASC LIMIT $so_luong";
            $stmt = $conn->prepare($sql);
            $stmt->execute();
        } catch (Exception $e)
        {
            $error = "loi_xuat_hang";
        }
    }
    header('Location: /xuat_hang?'.$error.'');
    exit;
}
$title = "Xuất đơn hàng";
require 'site/widget/header.php';
$sqlAll = "SELECT COUNT(`id`) FROM `sale_sanpham_03` WHERE `ma_so_xuat`=0 AND trang_thai=0 AND (type_dh<1 OR type_dh>8) ORDER BY `id` ASC";
$stmt = $conn->query($sqlAll);
$num_rows = $stmt->fetchColumn();
?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div id="id_button_td">
                <h3 class="mt-3 ml-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_TaoDonHang" aria-expanded="false" aria-controls="collapseTwo">Xuất đơn hàng</a>
                </h3>
            </div>
            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_TaoDonHang" class="card collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-row">
                                        <div class="col-md mb-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text">Team</span>
                                                </div>
                                                <select class="custom-select flex-grow-1" name="in_team" id="check_team_on" onchange="check_member_team()">
                                                    <?php
                                                    $stmt1 =  $conn->prepare("SELECT * FROM danh_sach_team ORDER BY id ASC");
                                                    $stmt1->execute();
                                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                    echo '<option value="0">...</option>';
                                                    foreach ($list_code as $show) {
                                                        $selected_ck_2 = $arr_t[1] == $show['id'] ? 'selected' : '';
                                                        echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name_team'] . '</option>';
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group mb-3  col-md-12">
                                            <input class="form-control h38" type="number" max="<?= $num_rows ?>" name="so_luong" id="id_js_sl" placeholder="Trong kho còn <?= $num_rows ?> đơn hàng chưa xuất" autocomplete="off" required></input>
                                        </div>
                                        <div class="form-group  mb-3 col-md-12">
                                            <textarea class="form-control" rows="2" name="ghi_chu" id="js_ghi_chu" placeholder="Mô tả đơn hàng (ghi chú)"></textarea>
                                        </div>
                                    </div>
                                    <div class="text-xs-right">
                                        <button type="submit" name="xac_nhan" id="button_xn_k" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-12 mt-2">
                <div class="card">
                    <div class="card-header alert-info">
                        <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                            Danh sách đơn hàng đã xuất - <a href="/xuat_hang&admin&tb&tb" title="Xem đơn chưa xuất" target="_blank" style="color: yellow;">(đơn mới)</a>
                        </div>
                    </div>
                    <div class="">
                        <div class="table-responsive flex-row flex-nowrap">
                            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="text-align:center; width:5%;">#</th>
                                        <th style="text-align:center;">Người xuất</th>
                                        <th style="text-align:center;">Số đơn</th>
                                        <th style="text-align:center;">Mã xuất</th>
                                        <th style="text-align:center;">Ghi chú</th>
                                        <th style="text-align:center;">Thời gian</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sqlAll = "SELECT COUNT(`mem`) FROM `sale_sanpham_03_xuat`";
                                    $stmt5 = $conn->query($sqlAll);
                                    $total_records  = $stmt5->fetchColumn();
                                    if($id_level < 8)
                                    {
                                        $total_records = 1;
                                    }
                                    $limit = $member['limit_page'] > 0 ? $member['limit_page'] : 20;
                                    $total_page = ceil($total_records / $limit);
                                    $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 0;
                                    $_GET['page'] = $_GET['page'] > 0 ? $_GET['page'] : 0;
                                    if ($total_page > 0) {
                                        $total_page_max = $total_page - 1;
                                    } else {
                                        $total_page_max = $total_page;
                                    }
                                    $_GET['page'] = $total_page_max < $_GET['page'] ? $total_page_max : $_GET['page'];
                                    $start_page = $_GET['page'] * $limit;
                                    $num_1 = 0;

                                    $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_03_xuat ORDER BY id DESC LIMIT $start_page, $limit");
                                    $stmt1->execute(array());
                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                    $num = 0;
                                    foreach ($list_code as $show_fp) {
                                        $num_1 = $num_1 + 1;
                                        $num = $num_1 + $_GET['page'] * $limit;
                                        $ngay_show = date('Ymd', $show_fp['time_xuat']) == $time_num ? date('H:i:s', $show_fp['time_xuat']) : date('d-m-Y', $show_fp['time_xuat']);
                                        $show_fp['ghi_chu'] = $show_fp['ghi_chu'] == '' ? '...' : $show_fp['ghi_chu'];
                                        echo '<tr>
                                        <td style="text-align:center;">' . $num .'</td>
                                        <td style="text-align:center;">' . ucwords(sql_member($show_fp['mem'], 'name')) . '</td>
                                        <td style="text-align:center;">' . number_format($show_fp['so_luong']) . '</td>
                                        <td style="text-align:center;"><a href="/xuat_hang&ghtk=' . $show_fp['code_xuat'] . '&tb" title="Bấm để xem danh sách" target="_blank">' . $show_fp['code_xuat'] . '</a></td>                              
                                        <td style="text-align:center;">' . $show_fp['ghi_chu'] . '</td>
                                        <td style="text-align:center;" title="' . date('H:i:s', $show_fp['time_xuat']) . '">' . $ngay_show . '</td>
                                        </tr>';
                                        if($id_level< 8)
                                        {
                                            break;
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php
                load_page(_sql02($_SERVER['REQUEST_URI']), $total_page, $limit, $total_records, $total_page_max);
                load_dialog($total_page_max, $member['id']);
                ?>
            </div>
        </div>
    </div>
</main>
<script>
    function check_member_team() {
        var team = document.getElementById("check_team_on").value;
        $.post("xuat_hang", {
            post_team: team
        }, function(data, status) {
            // js_ghi_chu
            var a1 = data.split('|');
            document.getElementById("id_js_sl").placeholder = "Trong kho còn " + a1[0] + " đơn hàng chưa xuất";
            if (a1[0] < 1) {
                $("#id_js_sl").prop("disabled", true);
            } else {
                $("#id_js_sl").prop("disabled", false);
            }
            document.getElementById("js_ghi_chu").value = "Team: " + a1[1];
            //alert(data);
        });
    }
</script>