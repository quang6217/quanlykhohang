<?php
if (isset($_POST['question'])) {
    $nd = _sql($_POST['question']);
    $tim_kiem = '`key_a` LIKE \'%' . $nd . '%\'';
    $stmt1 =  $conn->prepare("SELECT * FROM vn_chat_bot_question WHERE $tim_kiem ORDER BY id DESC LIMIT 100"); //Có thể chọn RAND ()
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    echo '<select multiple class="custom-select flex-grow-1" id="id_p_goi_y" onchange="p_goi_y()">';
    foreach ($list_code as $show) {
        echo '<option value="' . $show['key_a'] . '">' . $show['key_a'] . '</option>';
    }
    echo '</select>';
    exit;
}
if (isset($_POST['xac_nhan'])) {
    $tra_loi = _sql($_POST['tra_loi']);
    $cau_hoi = _sql($_POST['cau_hoi']);
    $y_nghia = _sql($_POST['y_nghia']);    
    $y_nghia = $y_nghia==""?$tra_loi:$y_nghia;
    try {
        if($tra_loi!=""){
            $sql = "INSERT INTO `vn_chat_bot_question`(`key_a`) VALUES ('$tra_loi')";
            $conn->exec($sql);
        }        
    } catch (Exception $e) {
    }

    $stmt =  $conn->prepare("SELECT * FROM vn_chat_bot_question WHERE key_a =:key_a" );
    $stmt->execute(array(":key_a" => $tra_loi));
    $tla= $stmt->fetch(PDO::FETCH_ASSOC);
    $id_tr = (int)$tla['id'];
    $sql = "INSERT INTO `vn_chat_data`(`cau_hoi`, `id_mau`, `y_nghia`) VALUES ('$cau_hoi',$id_tr,'$y_nghia')";
    $conn->exec($sql);


    header('Location: /rep_chat_1');
    exit;
}
$title = "Trả lời khách hàng";
require 'site/widget/header.php';

$stmt1 =  $conn->prepare("SELECT * FROM 00_cau_hoi_kh WHERE da_rep=0 ORDER BY id ASC LIMIT 1"); //Có thể chọn RAND ()
$stmt1->execute();
$list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
foreach ($list_code as $show) {
    $cau_hoi = $show['noi_dung'];
    $sql = "UPDATE 00_cau_hoi_kh SET da_rep=1 WHERE id='" . $show['id'] . "'";
    $stmt = $conn->prepare($sql);
    $stmt->execute();
}

$stmt1 =  $conn->prepare("SELECT * FROM 00_cau_hoi_kh WHERE da_rep=1 ORDER BY id DESC LIMIT 3"); //Có thể chọn RAND ()
$stmt1->execute();
$list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
$cau_hoi_gan_nhat = '';
foreach ($list_code as $show) {
    $cau_hoi_gan_nhat = $show['noi_dung'] . "\n" . $cau_hoi_gan_nhat;
}
?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_TaoDonHang" class="card collapse show" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-row">
                                        <div class="form-group  mb-3 col-md-12" id="id_edit_ghi_chu">
                                            <textarea class="form-control" rows="4" placeholder="Đã trả lời gần nhất" disabled><?= $cau_hoi_gan_nhat ?></textarea>
                                        </div>

                                        <div class="input-group mb-3 col-md-12">
                                            <input class="form-control h38" name="cau_hoi" value="<?= $cau_hoi ?>" placeholder="Câu đang hỏi" autocomplete="off"></input>
                                        </div>

                                        <div class="input-group mb-3 col-md-12">
                                            <input class="form-control h38" name="tra_loi" id="id_tra_loi" onkeyup="fu_tra_loi()" onchange="fu_tra_loi()" placeholder="Giải thích ý nghĩa câu hỏi" autocomplete="off"></input>
                                        </div>

                                        <div class="form-group mb-3  col-md-12" id="goi_y_tra_loi">
                                        </div>

                                        <div class="input-group mb-3 col-md-12">
                                            <input class="form-control h38" name="y_nghia" value="" placeholder="Ý nghĩa câu hỏi" autocomplete="off"></input>
                                        </div>

                                    </div>
                                    <div class="text-xs-right">
                                        <button type="submit" name="xac_nhan" id="button_xn_k" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                function post_tra_loi(dia_chi) {
                    var post_dc = encodeURI("question=" + dia_chi);
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() {
                        var html = xmlhttp.responseText.trim();
                        var post_id = html[1];
                        var post_string = html[0];
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            if (html.length > 3) {
                                document.getElementById("goi_y_tra_loi").innerHTML = html;
                            }
                        }
                    };
                    xmlhttp.open("POST", "rep_chat_1", true);
                    xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    xmlhttp.send(post_dc);
                }

                function p_goi_y() {                    
                    document.getElementById("id_tra_loi").value = document.getElementById("id_p_goi_y").value;
                    document.getElementById("goi_y_tra_loi").innerHTML = "";                                   
                }

                function fu_tra_loi() {
                    var tl = document.getElementById("id_tra_loi").value;
                    if (tl.length > 1) {
                        post_tra_loi(tl);
                    }
                }
            </script>