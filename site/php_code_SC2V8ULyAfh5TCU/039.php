<?php
$title = 'Lịch sữ giao dịch';
require 'site/widget/header.php';
?>
<main class="content">
  <div class="container-fluid p-0">
    <div class="row">
      <div class="col-12">
        <div class="card">
          <div class="card-header alert-info d-flex justify-content-between">
            <div class="p-2 bd-highlight" style="color:white; font-size: 18px;">Lịch sữ giao dịch</div>
            <a class="p-2 bd-highlight" style="color:white; font-size: 18px;" data-toggle="dropdown" href="#"><i data-feather="delete"></i></a></a>
            <div class="dropdown-menu">
              <a class="dropdown-item" href="#" style="color:blue;">Tháng này</a>
              <a class="dropdown-item" href="#" style="color:blue;">Tháng trước</a>
            </div>
          </div>
          <div class="table-responsive">
            <table class="table table-bordered table-striped mb-0">

              <thead>
                <tr>
                  <th style="width:5%; text-align:center;">#</th>
                  <th style="width:10%; text-align:center;">Số tiền</th>
                  <th style="width:10%; text-align:center;">Số dư</th>
                  <th style="width:15%; text-align:center;">Đối tác</th>
                  <th style="width:10%; text-align:center;">Giao dịch</th>
                  <th style="width:20%; text-align:center;">Ghi chú</th>
                  <th style="width:20%; text-align:center;">Thời gian</th>
                </tr>
              </thead>
              <tbody>

                <?php
                $sqlAll = "SELECT COUNT(`id`) FROM `lich_su_giao_dich` WHERE `id_chuyen`='$id_member' OR `id_nhan`='$id_member'";
                $stmt5 = $conn->query($sqlAll);
                $total_records  = $stmt5->fetchColumn();
                $limit = $member['limit_page'] > 0 ? $member['limit_page'] : 20;
                $total_page = ceil($total_records / $limit);
                $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 0;
                $_GET['page'] = $_GET['page'] > 0 ? $_GET['page'] : 0;
                if ($total_page > 0) {
                  $total_page_max = $total_page - 1;
                } else {
                  $total_page_max = $total_page;
                }
                $_GET['page'] = $total_page_max < $_GET['page'] ? $total_page_max : $_GET['page'];
                $start_page = $_GET['page'] * $limit;

                $num_1 = 0;
                $stmt1 =  $conn->prepare("SELECT * FROM lich_su_giao_dich WHERE  `id_chuyen`='$id_member' OR `id_nhan`='$id_member' ORDER BY id DESC LIMIT $start_page, $limit");
                $stmt1->execute(array());
                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                foreach ($list_code as $show_email) {
                  $num_1 = $num_1 + 1;
                  $num = $num_1 + $_GET['page'] * $limit;
                  if ($show_email['id_chuyen'] == $id_member) {
                    $so_tien = 0 - $show_email['so_tien'];
                    $doi_tac = $show_email['id_nhan'];
                    $so_du = $show_email['so_du_chuyen'];
                  } else {
                    $so_tien = $show_email['so_tien'];
                    $doi_tac = $show_email['id_chuyen'];
                    $so_du = $show_email['so_du_nhan'];
                  }
                  if ($so_tien > 0) {
                    $so_tien = '+' . number_format($so_tien, 0);
                    $mau_sac = 'color:blue;';
                  } else {
                    $so_tien = number_format($so_tien, 0);
                    $mau_sac = 'color:red;';
                  }
                  if ($so_du > 0) {
                    $so_du = '+' . number_format($so_du, 0);
                  } else {
                    $so_du = number_format($so_du, 0);
                  }
                  echo '<tr>
        <td style="text-align:center; ' . $mau_sac . '">' . $num . '.</td>
        <td style="text-align:center; ' . $mau_sac . '">' . $so_tien . 'đ</td>
        <td style="text-align:center; ' . $mau_sac . '">' . $so_du . 'đ</td>
        <td style="text-align:center; ' . $mau_sac . '">' . sql_member($doi_tac, 'name') . '</td>
        <td style="text-align:center; ' . $mau_sac . '">' . sql_giao_dich($show_email['loai_giao_dich']) . '</td>        
        <td style="text-align:center; ' . $mau_sac . '" title="' . $show_email['ghi_chu'] . '">' . substr($show_email['ghi_chu'], 0, 30) . '</td>
        <td class="table-action"  style="text-align:center; ' . $mau_sac . '">' . date('H:i:s d-m-Y', $show_email['thoi_gian']) . '</td>
        </tr>';
                }
                ?>

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>

  <?php load_page($_SERVER['REQUEST_URI'], $total_page, $limit, $total_records, $total_page_max);
  load_dialog($total_page_max, $member['id']); ?>

</main>
<script src="/js/clipboard/dist/clipboard.min.js"></script>

<script>
  var btns = document.querySelectorAll('button');
  var clipboard = new ClipboardJS(btns);

  clipboard.on('success', function(e) {
    console.log(e);
  });

  clipboard.on('error', function(e) {
    console.log(e);
  });
</script>

<script>
  $(function() {
    $('.sidebar-toggle.d-flex.mr-2').click(function() {
      $('footer.footer').toggleClass('no-padding');
    });
  });
</script>