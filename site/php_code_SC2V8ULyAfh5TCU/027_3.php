<?php
$title = "Các câu hỏi đã được dạy";
require 'site/widget/header.php';
?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-12 mt-2">
                <div class="card">
                    <div class="card-header alert-info">
                        <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                            Các câu hỏi đã học
                        </div>
                    </div>
                    <div class="">
                        <div class="table-responsive flex-row flex-nowrap">
                            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="text-align:center; width:5%;">#</th>
                                        <th style="text-align:center;">Member</th>
                                        <th style="text-align:center;">Ý câu hỏi</th>
                                        <th style="text-align:center;">Từ khóa</th>
                                        <th style="text-align:center;">Trả lời</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $sqlAll = "SELECT COUNT(`mem`) FROM `vn_chat_bot`";
                                    $stmt5 = $conn->query($sqlAll);
                                    $total_records  = $stmt5->fetchColumn();
                                    $limit = $member['limit_page'] > 0 ? $member['limit_page'] : 20;
                                    $total_page = ceil($total_records / $limit);
                                    $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 0;
                                    $_GET['page'] = $_GET['page'] > 0 ? $_GET['page'] : 0;
                                    if ($total_page > 0) {
                                        $total_page_max = $total_page - 1;
                                    } else {
                                        $total_page_max = $total_page;
                                    }
                                    $_GET['page'] = $total_page_max < $_GET['page'] ? $total_page_max : $_GET['page'];
                                    $start_page = $_GET['page'] * $limit;
                                    $num_1 = 0;

                                    $stmt1 =  $conn->prepare("SELECT * FROM vn_chat_bot ORDER BY id DESC LIMIT $start_page, $limit");
                                    $stmt1->execute(array());
                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                    $num = 0;
                                    foreach ($list_code as $show_fp) {
                                        $arr = (array) $show_fp;
                                        $num_1 = $num_1 + 1;
                                        $num = $num_1 + $_GET['page'] * $limit;
                                        $ngay_hien_tai = date('d/m/Y', $time_php);
                                        $ngay_them = date('d/m/Y', $show_fp['time_add']);
                                        $ngay_show = $ngay_them == $ngay_hien_tai ? date('H:i:s', $show_fp['time_add']) : $ngay_them;
                                        $sho_ghi = _sql($show_fp['ghi_chu']) != '' ? _sql($show_fp['ghi_chu']) : _sql($show_fp['address_detail']);
                                        $sho_ghi_a = mb_substr($sho_ghi, 0, 20, 'UTF-8');
                                        if ($id_member == $show_fp['mem'] or $id_level > 5) {
                                            $quyen_edit = '<a href="don_hang&edit_id=' . $show_fp['id'] . '"><i class="align-middle" data-feather="edit" style="color:#495057"></i></a>
                                            <a href="' . _sql03($_SERVER['REQUEST_URI']) . '&delfp=' . $show_fp['id'] . '" onclick="return confirm(\'#' . $num . ' - Đơn hàng của: ' . $show_fp['nameKH'] . ', sẽ bị xóa?\')"><i class="align-middle" data-feather="trash" style="color:#495057"></i></a>';
                                            $d_sdt = $show_fp['sdt'];
                                        } else {
                                            $quyen_edit = '<i class="align-middle" data-feather="alert-circle"></i>';
                                            $d_sdt = substr($show_fp['sdt'], 0, 7) . '***';
                                        }
                                        echo '<tr>
                                        <td style="text-align:center;">' . $num . '</td>
                                        <td style="text-align:center;">' . ucwords(sql_member($show_fp['mem'], 'name')) . '</td>
                                        <td style="text-align:center;">' . $show_fp['question'] . '</td>                                                                                
                                        <td style="text-align:center;">' . $show_fp['key_a'] . '</td>                                                                   
                                        <td style="text-align:center;">' . $show_fp['key_full'] . '</td>
                                        </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <?php
                load_page(_sql02($_SERVER['REQUEST_URI']), $total_page, $limit, $total_records, $total_page_max);
                load_dialog($total_page_max, $member['id']);
                ?>
            </div>
        </div>
    </div>
</main>