<?php
if ($id_level == 5) {
    header('Location: /');
    exit;
}
function get_tong_tien_chi($id_team,$tim_kiem_chi_tieu)
{
    global $conn;
    $stmt1 =  $conn->prepare("SELECT doi_tac ,sum(so_tien) as `tien_chi` FROM `ke_toan_03` where mem_xoa =0 and  doi_tac = $id_team and so_tien <0 and $tim_kiem_chi_tieu
    GROUP by doi_tac limit 1");
    $stmt1->execute();
    return $stmt1->fetch()["tien_chi"];

}

function _sql_date($t)
{
    return preg_replace('/[^0-9-]/', '', $t);
}
if (isset($_GET['search'])) {
    if ($_GET['search'] == 0) {
        $_SESSION['tim_kiem_list_hang'] = '1=1';
        $_SESSION['tim_kiem_ngay_thang'] = '1=1';
        $_SESSION['data_tim_kiem'] = '';
        $_SESSION['data_ngay_thang'] = '';
        header('Location: /tk_don_team');
        exit;
    }
}
if (isset($_POST['button_xn_tim_kiem'])) {
    $data_nguon_khach = '1=1';
    $id_nguon_khach = (int)$_POST['id_nguon_khach'];
    if ($id_nguon_khach > 0) {
        $data_nguon_khach = 'nguonKH=' . $id_nguon_khach;
    }
    $data_status_ly_do = '1=1';
    $id_status_ly_do = (int)$_POST['id_status_ly_do'];
    if ($id_status_ly_do > 0) {
        if ($id_status_ly_do < 1000) {
            $data_status_ly_do = 'type_dh=' . $id_status_ly_do;
        } else {
            $id_stt = $id_status_ly_do - 1000;
            $data_status_ly_do = 'trang_thai=' . $id_stt;
        }
    }
    $a_date_bd = '1=1';
    if ($_POST['date_bd'] != '') {
        $i_date_bd = _sql_date($_POST['date_bd']);
        $date_bd = _sql_num($_POST['date_bd']);
        $a_date_bd = '(`time_num`>=' . (int)$date_bd . ')';
    }
    $a_date_kt = '1=1';
    if ($_POST['date_kt'] != '') {
        $i_date_kt = _sql_date($_POST['date_kt']);
        $date_kt = _sql_num($_POST['date_kt']);
        $a_date_kt = '(`time_num`<=' . (int)$date_kt . ')';
    }
    $_SESSION['data_tim_kiem'] = $id_nguon_khach . '|' . $id_status_ly_do . '|' . $i_date_bd . '|' . $i_date_kt;
    $_SESSION['tim_kiem_list_hang'] = $data_nguon_khach . ' AND ' . $data_status_ly_do . ' AND ' . $a_date_bd . ' AND ' . $a_date_kt;
    $_SESSION['data_ngay_thang'] = $i_date_bd . '|' . $i_date_kt;
    $_SESSION['tim_kiem_ngay_thang'] = $a_date_bd . ' AND ' . $a_date_kt;
}
if (!isset($_SESSION['tim_kiem_list_hang'])) {
    $_SESSION['tim_kiem_list_hang'] = '1=1';
    $_SESSION['tim_kiem_ngay_thang'] = '1=1';
    $class_show = '';
} else {
    if ($_SESSION['tim_kiem_list_hang'] != '1=1') {
        $arr_t = explode('|', $_SESSION['data_tim_kiem']);
        // $arr_t_ngay_thang = explode('|', $_SESSION['data_ngay_thang']);
        $class_show = 'show';
    }
}
if ($i_date_bd < 1) {
    $i_date_bd = date('Y-m', $time_php) . '-1';
}
if ($i_date_kt < 1) {
    $i_date_kt = date('Y-m-d', $time_php);
}
$timkiem = $_SESSION['tim_kiem_list_hang'];
$timkiem_ngay_thang = $_SESSION['tim_kiem_ngay_thang'];
$list_tk = 'active';
$list_tk_1 = 'show';
$title = "Đơn hàng đã lên team";
require 'site/widget/header.php'; ?>
<div class="content">
    <div class="row">
        <div class="col-12 mb-3" id="accordion">
            <div id="collapse_TaoDonHang" class="card collapse <?= $class_show ?>" aria-labelledby="headingTwo" data-parent="#accordion">
                <div class="card" style="margin-bottom: -0.5rem;">
                    <div class="card-body">
                        <form method="post">
                            <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12">
                                <div class="row">
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Nguồn KH</span>
                                            </div>
                                            <select class="custom-select flex-grow-1" name="id_nguon_khach">
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM admin_nguon_khach_hang WHERE an=0 ORDER BY id ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                echo '<option value="0">...</option>';
                                                foreach ($list_code as $show) {
                                                    $selected_ck_2 = $arr_t[0] == $show['id'] ? 'selected' : '';
                                                    echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Trạng thái</span>
                                            </div>
                                            <select class="custom-select flex-grow-1" name="id_status_ly_do">
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM admin_ly_do_chot_xit WHERE an=0 ORDER BY id ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                echo '<option value="0">...</option>';
                                                foreach ($list_code as $show) {
                                                    $selected_ck_2 = $arr_t[1] == $show['id'] ? 'selected' : '';
                                                    echo '<option value="' . $show['id'] . '" ' . $selected_ck_2 . '>' . $show['name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>                            
                            <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12 mb-3">
                                <div class="row">
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend"><span class="input-group-text">Ngày bắt đầu</span>
                                            </div>
                                            <input type="date" name="date_bd" value="<?= $arr_t[2] ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-md mb-3">
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Kết thúc</span>
                                            </div>
                                            <input type="date" name="date_kt" value="<?= $arr_t[3] ?>" class="form-control">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-xs-right">
                                <button type="submit" name="button_xn_tim_kiem" id="button_xn_tim_kiem" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card mb-3">
                <div class="card-header alert-info">
                    <h5 class="card-title mb-0  text-white fs_18 d-flex justify-content-between" style="font-weight: bold;">
                        <div>Đơn hàng đã lên team</div><i class="align-middle mr-2 fas fa-fw fa-search" data-toggle="collapse" data-target="#collapse_TaoDonHang"></i></small>
                    </h5>
                </div>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="text-align:center;">TEAM</th>
                            <th style="text-align:center;">Đơn</th>
                            <th style="text-align:center;">Tổng tiền</th>
                            <th style="text-align:center;">VAT</th>
                            <th style="text-align:center;">Giảm giá</th>
                            <th style="text-align:center;">Ship</th>
                            <th style="text-align:center;">Thực nhận</th>
                            <th style="text-align:center;">Chi phí</th>
                            <th style="text-align:center;">%</th>

                        </tr>
                    </thead>
                    <tbody id="top_online">
                        <?php
                        $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`tong_tien` ) as `total_tong_tien`, sum(`a`.`thuc_nhan` ) as `total_thuc_nhan`, sum(`a`.`vat` ) as `total_vat`, sum(`a`.`ship` ) as `total_ship`, sum(`a`.`giam_gia` ) as `total_giam_gia`, count(`a`.`id` ) as `total_dh` 
                        FROM `sale_sanpham_03` AS  `a` WHERE $timkiem AND trang_thai!=44 AND (type_dh<1 OR type_dh>8) GROUP BY `team` ORDER BY `id` ASC");
                        $stmt1->execute(array());
                        $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                        $tong_tien_chi = 0;
                        foreach ($list_code as $show_fp) {
                            $tien_chi = 0;
                            if($id_level > 0)
                            {
                                if(get_tong_tien_chi($show_fp['team'],$timkiem_ngay_thang)!= NULL )
                                {
                                    $tien_chi = get_tong_tien_chi($show_fp['team'],$timkiem_ngay_thang);
                                    $tong_tien_chi += $tien_chi;
                                }
                            }
                            
                            $s_don = $s_don + $show_fp['total_dh'];
                            $s_tien = $s_tien + $show_fp['total_tong_tien'];
                            $s_vat = $s_vat + $show_fp['total_vat'];
                            $s_giam = $s_giam + $show_fp['total_giam_gia'];
                            $s_ship = $s_ship + $show_fp['total_ship'];
                            $s_nhan = $s_nhan + $show_fp['total_thuc_nhan'];
                            $kiem_tra_thuc_nhan_dk =  (int)$show_fp['total_thuc_nhan']>0?($tien_chi*-100/(int)$show_fp['total_thuc_nhan']):0;
                            echo '<tr>                        
                            <td style="text-align:center;">' . sql_web($show_fp['team'],'danh_sach_team','name_team') . '</td>
                            <td style="text-align:center;">' . number_format($show_fp['total_dh'], 0) . '</td>
                            <td style="text-align:center;">' . number_format($show_fp['total_tong_tien']) . '</td>
                            <td style="text-align:center;">' . number_format($show_fp['total_vat']) . '</td>
                            <td style="text-align:center;">' . number_format($show_fp['total_giam_gia']) . '</td>
                            <td style="text-align:center;">' . number_format($show_fp['total_ship']) . '</td>
                            <td style="text-align:center;">' . number_format($show_fp['total_thuc_nhan']) . '</td>
                            <td style="text-align:center;">'.number_format($tien_chi*-1).'</td>
                            <td style="text-align:center;">'.number_format($kiem_tra_thuc_nhan_dk).'%</td>
                            </tr>';
                        }
                        $kiem_tra_Tong_thuc_nhan_dk =  (int)$s_nhan>0?($tong_tien_chi*-100/$s_nhan):0;
                        echo '<tr>                        
                          <td style="text-align:center; color:red">Tổng</td>
                          <td style="text-align:center; color:red">' . number_format($s_don) . '</td>
                          <td style="text-align:center; color:red">' . number_format($s_tien) . '</td>
                          <td style="text-align:center; color:red">' . number_format($s_vat) . '</td>
                          <td style="text-align:center; color:red">' . number_format($s_giam) . '</td>
                          <td style="text-align:center; color:red">' . number_format($s_ship) . '</td>
                          <td style="text-align:center; color:red">' . number_format($s_nhan) . '</td>
                          <td style="text-align:center; color:red">'.number_format($tong_tien_chi*-1).'</td>
                          <td style="text-align:center; color:red">'.number_format($kiem_tra_Tong_thuc_nhan_dk).'%</td>
                          </tr>';
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>