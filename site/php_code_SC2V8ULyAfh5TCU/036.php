<?php
if (isset($_POST['xac_nhan'])) {
    $ma_hang = (int)$_POST['ma_hang'];
    $stmt =  $conn->prepare("SELECT * FROM vn_san_pham WHERE id =:id");
    $stmt->execute(array(":id" => $ma_hang));
    $sanp = $stmt->fetch(PDO::FETCH_ASSOC);
    $mau = explode('|', $sanp['mau_sac']);
    $i_mau = count($mau);
    $tong_sl_add = 0;
    $code_check = rand(10000000000, 99999999999999);
    for ($i = 0; $i < $i_mau; $i++) {
        $size = explode('|', $sanp['size']);
        $i_size = count($size);
        for ($y = 0; $y < $i_size; $y++) {
            $name_post = 'name_' . $mau[$i] . '_' . $size[$y];
            $gia_tri = isset($_POST[$name_post]) ? (int)$_POST[$name_post] : 0;
            $mau_sac = (int)$mau[$i];
            $size_a = (int)$size[$y];
            if ($gia_tri != 0) {
                //Lưu lại bản log để kiểm soát tốt hơn                   
                $sql = "INSERT INTO `vn_ton_kho_log_add`(`mem`, `team`, `ma_hang`, `mau_sac`, `size`, `so_luong`, `code_check`, `time_add`) VALUES ($id_member,$id_team,$ma_hang,$mau_sac,$size_a,$gia_tri,$code_check,$time_php)";
                $conn->exec($sql);
                $tong_sl_add = $tong_sl_add + $gia_tri;
                $stmt =  $conn->prepare("SELECT * FROM vn_ton_kho WHERE team=$id_team AND ma_hang =$ma_hang AND mau_sac=$mau_sac AND size=$size_a");
                $stmt->execute();
                $kho_hang = $stmt->fetch(PDO::FETCH_ASSOC);
                if ($kho_hang['id'] > 0) {
                    $sql = "UPDATE vn_ton_kho SET so_luong=so_luong+$gia_tri WHERE id='" . $kho_hang['id'] . "'";
                    $stmt = $conn->prepare($sql);
                    $stmt->execute();
                } else {
                    $sql = "INSERT INTO `vn_ton_kho`(`team`, `ma_hang`, `mau_sac`, `size`, `so_luong`) VALUES ($id_team,$ma_hang,$mau_sac,$size_a,$gia_tri)";
                    $conn->exec($sql);
                }
            }
        }
    }
    if ($tong_sl_add > 0) {
        $ghi_note = _sql($_POST['ghi_chu_qsj']);
        $sql = "INSERT INTO `vn_ton_kho_log_all`(`mem`, `team`, `ma_hang`, `code`, `so_luong`, `notes`, `time_add`) VALUES ($id_member,$id_team,$ma_hang,$code_check,$tong_sl_add,'$ghi_note',$time_php)";
        $conn->exec($sql);
    }
    header('Location: /kho_hang');
    exit;
}

if (isset($_GET['id_hang'])) {
    $id_ha = (int)$_GET['id_hang'];
    echo '<div class="input-group mb-3 col-md-12">
    <div class="input-group-prepend">
        <span class="input-group-text">Mã hàng</span>
    </div>
    <select name="ma_hang" id="id_hang" class="custom-select flex-grow-1" onchange="show_id_hang()">
        <option>.........</option>';
    // $stmt1 =  $conn->prepare("SELECT * FROM vn_san_pham WHERE team=$id_team  ORDER BY id ASC");
    $stmt1 =  $conn->prepare("SELECT * FROM vn_san_pham ORDER BY id ASC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $num = 0;
    foreach ($list_code as $show) {
        $selected_a = $_GET['id_hang'] == $show['id'] ? 'selected' : '';
        echo '<option value="' . $show['id'] . '" ' . $selected_a . '>' . $show['id'] . ' - ' . $show['name'] . '</option>';
    }
    echo '</select></div>';
    $stmt =  $conn->prepare("SELECT * FROM vn_san_pham WHERE id =:id");
    $stmt->execute(array(":id" => (int)$_GET['id_hang']));
    $sanp = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($sanp['id'] > 0) {
        $ar_mau = explode('|', $sanp['mau_sac']);
        for ($i = 0; $i < count($ar_mau); $i++) {
            $stmt =  $conn->prepare("SELECT * FROM vn_mau_sac WHERE id =:id");
            $stmt->execute(array(":id" => (int) $ar_mau[$i]));
            $mau = $stmt->fetch(PDO::FETCH_ASSOC);
            if ($mau['mau'] != "") {
                echo ' <div class="col-12 mt-2">
                <div class="card">
                <div class="card-header alert-info">
                    <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                    Màu ' . $mau['name'] . '
                    </div>
                </div>      
                <div class="card-body">
                <div class="form-row">';
                $ar_size = explode('|', $sanp['size']);
                for ($i1 = 0; $i1 < count($ar_size); $i1++) {
                    $stmt =  $conn->prepare("SELECT * FROM vn_size WHERE id =:id");
                    $stmt->execute(array(":id" => (int) $ar_size[$i1]));
                    $size = $stmt->fetch(PDO::FETCH_ASSOC);
                    echo '<div class="input-group mb-3 col-md-12">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Size ' . $size['name_1'] . '</span>
                            </div>
                        <input class="form-control h38" type="number" max="50000" value="" name="name_' . $mau['id'] . '_' . $size['id'] . '"></input>
                        </div>';
                }
                echo '</div></div></div></div>';
            }
        }
        echo '<div class="form-group  mb-3 col-md-12" id="id_edit_ghi_chu">
                <textarea class="form-control" rows="2" name="ghi_chu_qsj" placeholder="Thêm ghi chú của bạn"></textarea>
            </div>';
        exit;
    }
}

$title = "Kho hàng";
require 'site/widget/header.php';  ?>
<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div id="id_button_td">
                <h3 class="mt-3 ml-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_TaoDonHang" aria-expanded="false" aria-controls="collapseTwo">
                        Thêm hàng vào kho
                    </a>
                </h3>
            </div>

            <div id="id_button_ls">
                <h3 class="mt-3 ml-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_lichsu" aria-expanded="false" aria-controls="collapseTwo">
                        Lịch sữ thêm đơn hàng
                    </a>
                </h3>
            </div>

            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_TaoDonHang" class="card collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-row" id="show_du_lieu">
                                        <div class="input-group mb-3 col-md-12">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Mã hàng</span>
                                            </div>
                                            <select name="ma_hang" id="id_hang" class="custom-select flex-grow-1" onchange="show_id_hang()">
                                                <option>.........</option>
                                                <?php
                                                //$stmt1 =  $conn->prepare("SELECT * FROM vn_san_pham WHERE team=$id_team ORDER BY id ASC");
                                                $stmt1 =  $conn->prepare("SELECT * FROM vn_san_pham ORDER BY id ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                $num = 0;
                                                foreach ($list_code as $show) {
                                                    echo '<option value="' . $show['id'] . '">' . $show['id'] . ' - ' . $show['name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="text-xs-right">
                                        <button type="submit" name="xac_nhan" id="button_xn_k" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_lichsu" class="card collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-header alert-info">
                                <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                                    Lịch sữ kho
                                </div>
                            </div>
                            <div class="">
                                <div class="table-responsive flex-row flex-nowrap">
                                    <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center; width:5%;">#</th>
                                                <th style="text-align:center;">Người thêm</th>
                                                <th style="text-align:center;">Mã hàng</th>
                                                <th style="text-align:center;">Số lượng</th>
                                                <th style="text-align:center;">Ghi chú</th>
                                                <th style="text-align:center;">Thời gian</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php                                            
                                            $stmt1 =  $conn->prepare("SELECT * FROM vn_ton_kho_log_all WHERE  team=$id_team ORDER BY id DESC LIMIT 20");
                                            $stmt1->execute(array());
                                            $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                            $num = 0;
                                            foreach ($list_code as $show_fp) {
                                                $num = $num + 1;
                                                $ngay_show = date('Ymd', $show_fp['time_add']) == $time_num ? date('H:i:s', $show_fp['time_add']) : date('d-m-Y', $show_fp['time_add']);
                                                $show_fp['ghi_chu'] = $show_fp['ghi_chu'] == '' ? '...' : $show_fp['ghi_chu'];
                                                echo '<tr>
                                                    <td style="text-align:center;">' . $num . '</td>                                        
                                                    <td style="text-align:center;">' . ucwords(sql_member($show_fp['mem'], 'name')) . '</td>                                        
                                                    <td style="text-align:center;">' . $show_fp['ma_hang'] . ' - ' . check_ma_hang($show_fp['ma_hang'], 'name') . '</td>                                        
                                                    <td style="text-align:center;">' . $show_fp['so_luong'] . '</td>                                                                      
                                                    <td style="text-align:center;">' . _sql($show_fp['notes']) . '</td> 
                                                    <td style="text-align:center;" title="' . date('H:i:s', $show_fp['time_add']) . '">' . $ngay_show . '</td>
                                                    </tr>';
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php                       
            $stmt1 =  $conn->prepare("SELECT * FROM vn_san_pham ORDER BY id DESC");  //Nếu muốn phân loại theo team thì cần xài chức năng team
            $stmt1->execute();
            $list_code01 = $stmt1->fetchALL(PDO::FETCH_ASSOC);            

            foreach ($list_code01 as $show_id) {
                $ma_hang = (int)$show_id['id'];                
                $sqlAll = "SELECT SUM(`so_luong`) FROM `vn_ton_kho` WHERE `team`=$id_team AND `ma_hang`=$ma_hang";
                $stmt = $conn->query($sqlAll);
                $tong_sl_ton = $stmt->fetchColumn();

                
                if ($tong_sl_ton != 0) {
                    $stmt =  $conn->prepare("SELECT * FROM vn_san_pham WHERE id =:id");
                    $stmt->execute(array(":id" => $ma_hang));
                    $sanp = $stmt->fetch(PDO::FETCH_ASSOC);
            ?>
                    <div class="col-12 mt-2">
                        <div class="card">
                            <div class="card-header alert-info">
                                <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                                    <?= $sanp['name'] ?> - Mã <?= $sanp['id'] ?> trong kho còn: <?= number_format($tong_sl_ton, 0) ?>/sp
                                </div>
                            </div>
                            <div class="">
                                <div class="table-responsive flex-row flex-nowrap">
                                    <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center; width:5%;">#</th>
                                                <th style="text-align:center;">Màu sắc</th>
                                                <?php
                                                $ar_size = explode('|', $sanp['size']);
                                                for ($i1 = 0; $i1 < count($ar_size); $i1++) {
                                                    $stmt =  $conn->prepare("SELECT * FROM vn_size WHERE id =:id");
                                                    $stmt->execute(array(":id" => (int) $ar_size[$i1]));
                                                    $size = $stmt->fetch(PDO::FETCH_ASSOC);
                                                    echo '<th style="text-align:center;">Size ' . $size['name_1'] . '</th>';
                                                }
                                                ?>
                                                <th style="text-align:center;">Tổng</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $mau_sac = explode('|', $sanp['mau_sac']);
                                            $num = 0;
                                            for ($i1 = 0; $i1 < count($mau_sac); $i1++) {
                                                $num++;
                                                $stmt =  $conn->prepare("SELECT * FROM vn_mau_sac WHERE id =:id");
                                                $stmt->execute(array(":id" => (int) $mau_sac[$i1]));
                                                $mau_sac1 = $stmt->fetch(PDO::FETCH_ASSOC);
                                                echo '<tr>
                                            <td style="text-align:center;">' . $num . '</td>                                        
                                            <td style="text-align:center;">Màu ' . $mau_sac1['name'] . '</td>';
                                                $ar_size2 = explode('|', $sanp['size']);
                                                $tong_sl = 0;
                                                for ($i2 = 0; $i2 < count($ar_size2); $i2++) {
                                                    $stmt =  $conn->prepare("SELECT * FROM vn_size WHERE id =:id");
                                                    $stmt->execute(array(":id" => (int) $ar_size2[$i2]));
                                                    $size = $stmt->fetch(PDO::FETCH_ASSOC);
                                                    $id_mau = (int)$mau_sac1['id'];
                                                    $id_size = (int)$ar_size2[$i2];
                                                    $stmt1 =  $conn->prepare("SELECT * FROM vn_ton_kho WHERE team=$id_team AND ma_hang=$ma_hang AND mau_sac=$id_mau AND size = $id_size");
                                                    $stmt1->execute();
                                                    $ton_kho = $stmt1->fetch(PDO::FETCH_ASSOC);
                                                    echo '<th style="text-align:center;">' . number_format($ton_kho['so_luong'], 0) . '</th>';
                                                    $tong_sl = $tong_sl + $ton_kho['so_luong'];
                                                }
                                                echo '<td style="text-align:center;">' . number_format($tong_sl, 0) . '</td>';
                                                echo '</tr>';
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
            <?php }
            } ?>
            <script>
                function show_id_hang() {
                    var x = document.getElementById("id_hang");
                    var i = x.selectedIndex;
                    var id_hang = (x.options[i].value);
                    var xmlhttp;
                    if (window.XMLHttpRequest) {
                        xmlhttp = new XMLHttpRequest();
                    } else {
                        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    xmlhttp.onreadystatechange = function() {
                        if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                            var html_full = xmlhttp.responseText.trim();
                            document.getElementById("show_du_lieu").innerHTML = html_full;
                        }
                    };
                    xmlhttp.open("GET", "kho_hang&id_hang=" + id_hang, true);
                    xmlhttp.send();
                }
            </script>