<?php
if ($member['level'] < 8) {
    header('Location: /admin_nap_tien');
    exit;
}
if (isset($_POST['chuyen_tien_oke'])) {
    $id_nhan = (int) $_POST['id_nhan'];
    $so_tien = (int) $_POST['so_tien'];
    $noi_dung = $id_member.' - '._sql01($_POST['noi_dung']);
    $xac_nhan_pass = _sql01($_POST['xac_nhan_pass']);
    $ket_qua = lich_su_giao_dich($so_tien, 9, $id_nhan, 16, $noi_dung, '', 8, $time_php);
    if ($ket_qua == 9) {
        header('Location: /admin_nap_tien');
    } else {
        header('Location: /admin_nap_tien');
    }
    exit;
}
if (!isset($_POST['chuyen_tien'])) {
    $title = 'Nạp tiền';
    require 'site/widget/header.php';
?>
    <main class="content">
        <div class="container-fluid p-0">
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h5 class="card-title mb-0">Nạp tiền trực tiếp cho member</h5>
                        </div>
                        <div class="card-body">
                            <form method="post" action="<?= _sql01($_SERVER['REQUEST_URI']) ?>">
                                <div class="form-group">


                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Tổng tiền hệ thống:</span>
                                        </div>
                                        <input type="number" class="form-control" placeholder="<?= number_format(sql_member(9, 'money'), 0) ?>đ" disabled>
                                    </div>

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Số ID tài khoản</span>
                                        </div>
                                        <input type="number" min="1" step="1" class="form-control" name="id_nhan" autocomplete="off" value="" required>
                                    </div>

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Số tiền nạp</span>
                                        </div>
                                        <input type="number" class="form-control" name="so_tien" autocomplete="off" value="" required>
                                    </div>

                                    <div class="input-group mb-3">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text">Nội dung</span>
                                        </div>
                                        <input type="text" maxlength="50" class="form-control" name="noi_dung" value="<?= $member['name'] ?>" autocomplete="off" required>
                                    </div>
                                    <div class="text-xs-right">
                                        <button type="submit" name="chuyen_tien" class="btn btn-block btn-flat margin-top-10 btn-info">Kiểm tra</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                <?php } ?>

                <?php
                if (isset($_POST['chuyen_tien'])) {
                    $title = 'Xác nhận chuyển tiền';
                    require 'site/widget/header.php';
                    $id_nhan = (int) $_POST['id_nhan'];
                    $so_tien = (int) $_POST['so_tien'];
                    $noi_dung = _sql01($_POST['noi_dung']);
                    $xac_nhan_pass = _sql01($_POST['xac_nhan_pass']);
                    $ket_qua = lich_su_giao_dich($so_tien, 9, $id_nhan, 16, $noi_dung, '', 0, $time_php);
                    $khoa = '';
                    $input_gui = '';
                    $button_1 = '<button type="submit" name="chuyen_tien" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>';
                    if ($ket_qua == 'oke') {
                        $khoa = 'disabled';
                        $button_1 = '
                                <div class="d-flex justify-content-between">
                                <div><button class="btn btn-block btn-primary">Hủy</button></div>
                                <div><button type="submit" name="chuyen_tien_oke" class="btn btn-block btn-danger">Xác nhận giao dịch</button></div>
								</div>
    ';
                        $ket_qua = 'Hệ thống sẽ nạp: <font color=red><b>' . number_format($so_tien, 0) . 'đ</b></font> cho <font color=red><b>' . sql_member($id_nhan, 'name') . ',</b></font> bạn chắc chắn muốn tiếp tục?';
                        $input_gui = '  <input type="hidden" class="form-control" name="xac_nhan_pass" value="' . $xac_nhan_pass . '"> <input type="hidden" class="form-control" name="noi_dung" value="' . $noi_dung . '"> <input type="hidden" class="form-control" name="so_tien" value="' . $so_tien . '"> <input type="hidden" class="form-control" name="id_nhan" value="' . $id_nhan . '">';
                    } else {
                        $ket_qua = '<font color=red><b>' . $ket_qua . '</b></font>';
                        $xac_nhan_pass = '';
                    }
                ?>
                    <main class="content">
                        <div class="container-fluid p-0">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h5 class="card-title mb-0"><?= $ket_qua ?></h5>
                                        </div>
                                        <div class="card-body">
                                            <form method="post" action="<?= _sql01($_SERVER['REQUEST_URI']) ?>">
                                                <div class="form-group">


                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Số dư khả dụng</span>
                                                        </div>
                                                        <input type="number" class="form-control" placeholder="<?= number_format(sql_member($id_member, 'money'), 0) ?>đ" disabled>
                                                    </div>

                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Số ID tài khoản</span>
                                                        </div>
                                                        <input type="number" class="form-control" name="id_nhan" value="<?= $id_nhan ?>" autocomplete="off" required <?= $khoa ?>>
                                                    </div>

                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Số tiền chuyển</span>
                                                        </div>
                                                        <input type="number" class="form-control" name="so_tien" value="<?= $so_tien ?>" autocomplete="off" required <?= $khoa ?>>

                                                    </div>

                                                    <div class="input-group mb-3">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text">Nội dung</span>
                                                        </div>
                                                        <input type="text" maxlength="50" class="form-control" name="noi_dung" value="<?= $noi_dung ?>" autocomplete="off" required <?= $khoa ?>>

                                                    </div>

                                                    <div class="text-xs-right">
                                                        <?= $input_gui ?>
                                                        <?= $button_1 ?>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                <?php } ?>