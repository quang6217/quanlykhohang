<?php
function so_luong_nhap_vao_sp($id_sp)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT sum(so_luong) as `sl` FROM kho_nhap_vao WHERE ma_kem =:ma_kem");
    $stmt->execute(array(":ma_kem" => (int) $id_sp));
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    return $member["sl"];
}
function so_luong_kho_ban_sp($id_sp)
{
    global $conn;
    $sql = "SELECT * from kho_ban_ra WHERE ma_kem = " . $id_sp;
    $stmt1 =  $conn->prepare($sql);
    $stmt1->execute();
    return $stmt1->fetch()["so_luong"];
}  
function js052_kho_nhap_vao_tong($id, $sl){
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM kho_nhap_vao_tong WHERE san_pham =:san_pham");
    $stmt->execute(array(":san_pham" => (int) $id));
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    return $member[$sl];
}
function js052_kho_ban_ra($san_pham, $value){
    global $conn;
    $san_pham = (int)$san_pham;
    $sqlAll = "SELECT SUM(`so_luong`) FROM `kho_ban_ra`WHERE `san_pham`=$san_pham";
    $stmt = $conn->query($sqlAll);
    $num_rows = $stmt->fetchColumn();
    return $num_rows; }
function _sql_date($t)
{
    return preg_replace('/[^0-9-]/', '', $t);
}
function convert_to_date_int_kho($data)
{
  $year =  substr( $data,  0, 4);
  $month = substr( $data,  4, 2);
  $day = substr( $data,  6, 2);
  $datetime =  $year."/".$month."/".$day;
  return $timestamp = strtotime($datetime);
}
// if ($id_level < 5) {
//     header('Location: /');
//     exit;
// }
// Đếm tổng sản phẩm đã thêm vào kho 
$stmt1 =  $conn->prepare("SELECT * , sum(`a`.`so_luong` ) as `total_sl`, sum(`a`.`tien_nhap` ) as `total_tien` FROM `kho_nhap_vao` AS  `a` GROUP BY  `san_pham`  ORDER BY `total_sl` DESC");
$stmt1->execute(array());
$list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
foreach ($list_code as $show_code) {
    $id_san_pham = (int)$show_code['san_pham'];
    $tong_sl = (int)$show_code['total_sl'];
    $tong_tien = (int)$show_code['total_tien'];
    $stmt =  $conn->prepare("SELECT * FROM kho_nhap_vao_tong WHERE san_pham=$id_san_pham");
    $stmt->execute();
    $asan_pham = $stmt->fetch(PDO::FETCH_ASSOC);
    $ia_sp = (int)$asan_pham['id'];
    if ($ia_sp > 0) {
        $sql = "UPDATE kho_nhap_vao_tong SET sl='$tong_sl', tien='$tong_tien' WHERE id='$ia_sp'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
    } else {
        $sql = "INSERT INTO `kho_nhap_vao_tong`(`san_pham`, `sl`, `tien`) VALUES ($id_san_pham,$tong_sl,$tong_tien)";
        $conn->exec($sql);
    }
}
// Đếm tổng số sản phẩm đã bán
// $time_check = 0;
// // $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`sl` ) as `total_sl`, sum(`a`.`tien` ) as `total_tien` 
// // FROM `sale_sanpham_04` AS  `a` WHERE team=$id_team AND `time_num`>$time_check 
// // GROUP BY  `sp_02`  ORDER BY `total_sl` DESC");
// $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`sl` ) as `total_sl`, sum(`a`.`tien` ) as `total_tien` 
// FROM `sale_sanpham_04` AS  `a` WHERE `time_num`>$time_check 
// GROUP BY  `sp_02`  ORDER BY `total_sl` DESC");

// $stmt1->execute(array());
// $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);


// $num = 0;
// foreach ($list_code as $show_code) {
//     // var_dump($show_code['sp_01']);
//     // echo "<br>";
//     $ma_kem = (int)$show_code['sp_02'];
//     $id_san_pham = (int)$show_code['sp_01'];
//     $tong_sl = (int)$show_code['total_sl'];
//     $tong_tien = (int)$show_code['total_tien'];
//     $stmt =  $conn->prepare("SELECT * FROM kho_ban_ra WHERE ma_kem=$ma_kem");
//     $stmt->execute();
//     $asan_pham = $stmt->fetch(PDO::FETCH_ASSOC);
//     $ia_sp = (int)$asan_pham['id'];
//     if ($ia_sp > 0) {
//         $sql = "UPDATE kho_ban_ra SET  so_luong='$tong_sl', tong_tien_ban='$tong_tien' WHERE id='$ia_sp'";
//         $stmt = $conn->prepare($sql);
//         $stmt->execute();
//     } else {
//         $sql = "INSERT INTO `kho_ban_ra`(`san_pham`, `ma_kem`, `so_luong`, `tong_tien_ban`) VALUES ($id_san_pham,$ma_kem,$tong_sl,$tong_tien)";
//         $conn->exec($sql);
//     }
// }
kho_update();
if (isset($_POST['js_tk'])) {
    $san_pham = (int)$_POST['js_tk'];
    echo '<div class="table-responsive flex-row flex-nowrap">
            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
            <thead>
            <tr>
            <th style="text-align:center; width:5%;">#</th>
            <th style="text-align:center;">Mã kèm</th>
            <th style="text-align:center;">SL tồn</th>
            <th style="text-align:center;">Tiền tồn</th>
            <th style="text-align:center;">Lợi nhuận</th>
            </tr>
            </thead>
            <tbody>';


    $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`so_luong` ) as `total_sl`, sum(`a`.`tong_tien_ban` ) as `total_tien` FROM `kho_ban_ra` AS  `a` WHERE 
    san_pham=$san_pham GROUP BY  `ma_kem`  ORDER BY `id` ASC");
    $stmt1->execute(array());
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $show_fp) {
        $json_br_sl[$show_fp['ma_kem']] = (int)$show_fp['total_sl'];
        $json_br_tien[$show_fp['ma_kem']] = (int)$show_fp['total_tien'];
    }
    $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`so_luong` ) as `total_sl`, sum(`a`.`tien_nhap` ) as `total_tien` FROM `kho_nhap_vao` AS  `a` WHERE 
    san_pham=$san_pham GROUP BY  `ma_kem`  ORDER BY `id` ASC");
    $stmt1->execute(array());
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach ($list_code as $show_fp) {
        $json_nv_sl[$show_fp['ma_kem']] = (int)$show_fp['total_sl'];
        $json_nv_tien[$show_fp['ma_kem']] = (int)$show_fp['total_tien'];
    }


    $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_02 WHERE id_san_pham=$san_pham ORDER BY id ASC"); //Có thể chọn RAND ()
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $num = 0;
    foreach ($list_code as $show_fp) 
    {
        
        if(count(check_ten_combo($show_fp["id"])) == 0)
        {
            $num = $num + 1;
            $ton_sl = $json_nv_sl[$show_fp['id']] - $json_br_sl[$show_fp['id']];
            $ton_tien = $json_br_tien[$show_fp['id']] - $json_nv_tien[$show_fp['id']];
            $sl = $sl + $ton_sl;
            $tien = $tien + $ton_tien;
            $tien_ton = $ton_sl * sql_web($show_fp['id'], 'sale_sanpham_02', 'gia_nhap');
            $tien_ton_sum = $tien_ton + $tien_ton_sum;
            if ($id_level < 8) {
                $tien = 0;
                $ton_tien = 0;
                $tien_ton = 0;
                $tien_ton_sum = 0;
            }
            echo '<tr>
                <td style="text-align:center;">' . $num . '</td>
                <td style="text-align:center;">' . $show_fp['ma_kem'] . '</td>
                <td style="text-align:center;">  ' . number_format($ton_sl) . '</td>
                <td style="text-align:center;">  ' . number_format($tien_ton) . '</td>
                <td style="text-align:center;">' . number_format($ton_tien) . '</td>
                </tr>';
        }
        
    }
    $num++;
    echo '<tr>
            <td style="text-align:center; color:red">' . $num . '</td>
            <td style="text-align:center; color:red">Tổng cộng:</td>
            <td style="text-align:center; color:red">' . number_format($sl) . '</td>
            <td style="text-align:center; color:red">' . number_format($tien_ton_sum) . '</td>
            <td style="text-align:center; color:red">' . number_format($tien) . '</td>
            </tr>';
    echo '</tbody></table></div>';
    exit;
}

if (isset($_POST['js_br'])) {
    $san_pham = (int)$_POST['js_br'];
    echo '<div class="table-responsive flex-row flex-nowrap">
            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
            <thead>
            <tr>
            <th style="text-align:center; width:5%;">#</th>
            <th style="text-align:center;">Mã kèm</th>
            <th style="text-align:center;">Số lượng</th>
            <th style="text-align:center;">Tiền bán</th>
            </tr>
            </thead>
            <tbody>';


    $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`so_luong` ) as `total_sl`, sum(`a`.`tong_tien_ban` ) as `total_tien` 
    FROM `kho_ban_ra` AS  `a` WHERE san_pham=$san_pham GROUP BY `ma_kem` ORDER BY `ma_kem` ASC");
    $stmt1->execute(array());
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $num = 0;
    foreach ($list_code as $show_fp) {
        $num = $num + 1;
        $sl = $sl + $show_fp['total_sl'];
        $tien = $tien +  $show_fp['total_tien'];
        if ($id_level < 8) {
            $tien = 0;
            $show_fp['total_tien'] = 0;
        }
        echo '<tr>
            <td style="text-align:center;">' . $num . '</td>
            <td style="text-align:center;">' . sql_sanpham_02($show_fp['ma_kem'], 'ma_kem') . '</td>
            <td style="text-align:center;">' . number_format($show_fp['total_sl']) . '</td>
            <td style="text-align:center;">' . number_format($show_fp['total_tien']) . '</td>
            </tr>';
    }
    $num++;
    echo '<tr>
            <td style="text-align:center; color:red">' . $num . '</td>
            <td style="text-align:center; color:red">Tổng cộng:</td>
            <td style="text-align:center; color:red">' . number_format($sl) . '</td>                                                   
            <td style="text-align:center; color:red">' . number_format($tien) . '</td>
            </tr>';
    echo '</tbody></table></div>';
    exit;
}

if (isset($_POST['js_nv'])) {
    $san_pham = (int)$_POST['js_nv'];
    echo '<div class="table-responsive flex-row flex-nowrap">
            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
            <thead>
            <tr>
            <th style="text-align:center; width:5%;">#</th>
            <th style="text-align:center;">Mã kèm</th>
            <th style="text-align:center;">Số lượng</th>
            <th style="text-align:center;">Tiền nhập</th>
            </tr>
            </thead>
            <tbody>';
    $stmt1 =  $conn->prepare("SELECT * , sum(`a`.`so_luong` ) as `total_sl`, sum(`a`.`tien_nhap` ) as `total_tien` FROM `kho_nhap_vao` AS  `a` WHERE 
    san_pham=$san_pham GROUP BY  `ma_kem`  ORDER BY `ma_kem` ASC");
    $stmt1->execute(array());
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $num = 0;
    foreach ($list_code as $show_fp) {
        $num = $num + 1;
        $sl = $sl + $show_fp['total_sl'];
        $tien = $tien +  $show_fp['total_tien'];
        if ($id_level < 8) {
            $tien = 0;
            $show_fp['total_tien'] = 0;
        }
        echo '<tr>
            <td style="text-align:center;">' . $num . '</td>
            <td style="text-align:center;">' . sql_sanpham_02($show_fp['ma_kem'], 'ma_kem') . '</td>
            <td style="text-align:center;">' . number_format($show_fp['total_sl']) . '</td>
            <td style="text-align:center;">' . number_format($show_fp['total_tien']) . '</td>
            </tr>';
    }
    $num++;
    echo '<tr>
            <td style="text-align:center; color:red">' . $num . '</td>
            <td style="text-align:center; color:red">Tổng cộng:</td>
            <td style="text-align:center; color:red">' . number_format($sl) . '</td>
            <td style="text-align:center; color:red">' . number_format($tien) . '</td>
            </tr>';
    echo '</tbody></table></div>';
    exit;
}


if (isset($_POST['chi_tiet_add_kho'])) {
    $time_asss = (int)$_POST['chi_tiet_add_kho'];
    echo '<div class="table-responsive flex-row flex-nowrap">
            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
            <thead>
            <tr>
            <th style="text-align:center; width:5%;">#</th>
            <th style="text-align:center;">Mã kèm</th>
            <th style="text-align:center;">Số lượng</th>
            <th style="text-align:center;">Tổng tiền</th>
            </tr>
            </thead>
            <tbody>';
    $stmt1 =  $conn->prepare("SELECT * FROM kho_nhap_vao WHERE time_add=$time_asss ORDER BY id DESC LIMIT 200");
    $stmt1->execute(array());
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $num = 0;
    foreach ($list_code as $show_fp) {
        $num = $num + 1;
        $sl = $sl + $show_fp['so_luong'];
        if ($id_level < 8) {
            $tien = 0;
            $tien_na = 0;
        } else {
            $tien = $tien +  $show_fp['tien_nhap'];
            $tien_na = $show_fp['tien_nhap'];
        }
        echo '<tr>
            <td style="text-align:center;">' . $num . '</td>
            <td style="text-align:center;">' . sql_sanpham_02($show_fp['ma_kem'], 'ma_kem') . '</td>
            <td style="text-align:center;">' . number_format($show_fp['so_luong']) . '</td>
            <td style="text-align:center;">' . number_format($tien_na) . '</td>
            </tr>';
    }
    $num++;
    echo '<tr>
            <td style="text-align:center; color:red">' . $num . '</td>
            <td style="text-align:center; color:red">Tổng cộng:</td>
            <td style="text-align:center; color:red">' . number_format($sl) . '</td>
            <td style="text-align:center; color:red">' . number_format($tien) . '</td>
            </tr>';
    echo '<tr><td  colspan="4" style="text-align:center; color:blue">Tạo ' . date("H:i:s", $time_asss) . ' ngày ' . date("d-m-Y", $time_asss) . '</td></tr>';
    echo '</tbody></table></div>';
    exit;
}

if (isset($_POST['id_hang'])) {
    $id_ha = (int)$_POST['id_hang'];
    echo '<div class="input-group mb-3 col-md-12">
    <div class="input-group-prepend">
        <span class="input-group-text">Mã hàng</span>
    </div>
    <select name="ma_hang" id="id_hang" class="custom-select flex-grow-1" onchange="show_id_hang()">
        <option>.........</option>';
    $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_01 ORDER BY id ASC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $num = 0;
    foreach ($list_code as $show) {
        $selected_a = $_POST['id_hang'] == $show['id'] ? 'selected' : '';
        echo '<option value="' . $show['id'] . '" ' . $selected_a . '>' . $show['id'] . ' - ' . $show['name'] . '</option>';
    }
    echo '</select></div>';
    $id_sp = (int)$_POST['id_hang'];
    $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_02 WHERE id_san_pham=$id_sp ORDER BY id ASC"); //Có thể chọn RAND ()
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $num = 0;
    foreach ($list_code as $show) 
    {
        if(count(check_ten_combo($show["id"])) == 0)
        {
            if ($id_level < 8) {
                $tien_na = '';
            } else 
            {
                $tien_na = 'nhập: <font color=red>' . number_format($show['gia_nhap']) . '</font> - ';
            }
            $num++;
            echo '<div class="input-group mb-3 col-md-12">
                    <div class="input-group-prepend">
                        <span class="input-group-text">' . $show['ma_kem'] . '&nbsp;&nbsp;<small>(' . $tien_na . 'bán: <font color=red>' . number_format($show['gia_tien']) . '</font>)</small></span>
                    </div>
                <input class="form-control h38" type="number" max="50000" value="" name="nd_post_' . $num . '"></input>
                <input hidden class="form-control h38" name="id_post_' . $num . '" value="' . $show['id_san_pham'] . '|' . $show['id'] . '"></input>
                </div>';
            echo '</div></div></div></div>';
        }
       
    }
    echo '<div class="form-group  mb-3 col-md-12" id="id_edit_ghi_chu">
                <textarea class="form-control" rows="2" name="ghi_chu_qsj" placeholder="Thêm ghi chú của bạn"></textarea>
            </div>';
    exit;
}
if (isset($_POST['xac_nhan'])) 
{
    $so_luong_1 = 0;
    $tong_tien_1 = 0;
    for ($i = 1; $i < 100; $i++) {
        if ((int)$_POST['id_post_' . $i] == 0) {
            break;
        }
        $so_luong = (int)$_POST['nd_post_' . $i];
        $so_luong_1 = $so_luong_1 + $so_luong;
        $arr = explode('|', $_POST['id_post_' . $i]);
        $ma_hang = (int)$arr[0];
        $ma_kem = (int)$arr[1];
        $gia_nhap = sql_sanpham_02($ma_kem, 'gia_nhap');
        $tong_tien_nhap = $gia_nhap * $so_luong;
        $tong_tien_1 = $tong_tien_1 + $tong_tien_nhap;
        // echo 'Số lượng: ' . $so_luong . ' - Mã hàng: ' . $ma_hang . ' - Mã kèm: ' . $ma_kem . ' (' . $gia_nhap . ') - Thành tiền: ' . $tong_tien_nhap . '<br>';
        try {
            $sql = "INSERT INTO `kho_nhap_vao`(`san_pham`, `ma_kem`, `so_luong`, `tien_nhap`, `time_add`) VALUES 
            ($ma_hang,$ma_kem,$so_luong,$tong_tien_nhap,$time_php)";
            //echo $sql;
            $conn->exec($sql);
        } catch (Exception $e) {
        }
    }
    $ghi_chu = _sql01($_POST['ghi_chu_qsj']);
    // echo 'Ghi chú: ' . $ghi_chu . '<br>'; echo 'Tổng số: ' . $so_luong_1;
    try {
        $sql = "INSERT INTO `kho_nhap_vao_ls`(`mem`, `sanpham01`, `tong_sl`, `tong_tien`, `ghi_chu`, `time_add`) VALUES
         ('$id_member','$ma_hang','$so_luong_1','$tong_tien_1','$ghi_chu','$time_php')";
        $conn->exec($sql);
        header('Location: /kho_hang');
        exit;
    } catch (Exception $e) {
        echo 'Error';
        exit;
    }
}
$_SESSION['tim_kiem_ngay_xuat_kho'] = '1=1 AND 1=1';
$_SESSION['data_tim_kiem_ngay_xuat_kho'] = '';
$showhtml  = "";
if (isset($_POST['button_xn_tim_kiem'])) {
    $a_date_bd = '1=1';
    if ($_POST['date_bd'] != '') {
        $i_date_bd = _sql_date($_POST['date_bd']);
        $date_bd = _sql_num($_POST['date_bd']);
        $date_bd = convert_to_date_int_kho($date_bd);
        $a_date_bd = '(`time_xuat`>=' . (int)$date_bd .')';
    }
    $a_date_kt = '1=1';
    if ($_POST['date_kt'] != '') {
        $i_date_kt = _sql_date($_POST['date_kt']);
        $date_kt = _sql_num($_POST['date_kt']);
        $date_kt = convert_to_date_int_kho($date_kt) +86400;
        $a_date_kt = '(`time_xuat`<=' . (int)$date_kt . ')';
    }
    $_SESSION['tim_kiem_ngay_xuat_kho'] = $a_date_bd . ' AND ' . $a_date_kt ;
    $_SESSION['data_tim_kiem_ngay_xuat_kho'] = $i_date_bd.'|'.$i_date_kt;
    $showhtml  = " show";
}
if ($_SESSION['tim_kiem_ngay_xuat_kho'] != '1=1 AND 1=1')
{
    if ($_SESSION['data_tim_kiem_ngay_xuat_kho'] != '')
    {
        $arr_t = explode('|', $_SESSION['data_tim_kiem_ngay_xuat_kho']);
    }
}

$title = "Kho hàng";
require 'site/widget/header.php';  ?>

<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
        <!--  -->
            <!-- <div class="col-12 mb-3" id="accordion">
                <div id="collapse_TaoDonHang_xuat" class="card collapse <?php echo $showhtml; ?>" aria-labelledby="headingTwo" data-parent="#accordion">
                    <div class="card" style="margin-bottom: -0.5rem;">
                        <div class="card-body">
                            <form method="post">
                                <div class="demo-vertical-spacing-sm button-dropdown-input-group-demo col-md-12 mb-3">
                                    <div class="row">
                                        <div class="col-md mb-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend"><span class="input-group-text">Ngày bắt đầu</span>
                                                </div>
                                                <input type="date" name="date_bd" value="<?= $arr_t[0] ?>" class="form-control">
                                            </div>
                                        </div>
                                        <div class="col-md mb-3">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="input-group-text">Kết thúc</span>
                                                </div>
                                                <input type="date" name="date_kt" value="<?= $arr_t[1] ?>" class="form-control">
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="text-xs-right">
                                    <button  type="submit" name="button_xn_tim_kiem" id="button_xn_tim_kiem" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div> -->
        <!--  -->
            <div id="id_button_td">
                <h3 class="mt-3 ml-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_TaoDonHang" 
                    aria-expanded="false" aria-controls="collapseTwo" onclick = "myFunction(<?php echo $id_level; ?>)">
                        Thêm hàng vào kho 
                    </a>
                </h3>
            </div>

            <div id="id_button_ls">
                <h3 class="mt-3 ml-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_lichsu" aria-expanded="false" aria-controls="collapseTwo" onclick = "myFunction(<?php echo $id_level; ?>)">
                        Lịch sử thêm đơn hàng
                    </a>
                </h3>
            </div>
            <!-- <div id="id_button_ls">
                <h3 class="mt-3 ml-3 btn btn-info mr-2" style="float: left;">
                    <a class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse_xuatkho" aria-expanded="false" aria-controls="collapseTwo">
                        Lịch sử xuất kho
                    </a>
                </h3>
            </div> -->
            <!-- kiểm tra nếu quyền tk > 6 mới xuất hiện thêm hàng -->
            <?php if($id_level >6 || $id_level == 5)
            {
                ?>
            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_TaoDonHang" class="card collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-body">
                                <form method="post">
                                    <div class="form-row" id="show_du_lieu">
                                        <div class="input-group mb-3 col-md-12">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text">Mã hàng</span>
                                            </div>
                                            <select name="ma_hang" id="id_hang" class="custom-select flex-grow-1" onchange="show_id_hang()">
                                                <option>.........</option>
                                                <?php
                                                $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_01 ORDER BY id ASC");
                                                $stmt1->execute();
                                                $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                                $num = 0;
                                                foreach ($list_code as $show) {
                                                    echo '<option value="' . $show['id'] . '">' . $show['id'] . ' - ' . $show['name'] . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="text-xs-right">
                                        <button type="submit" name="xac_nhan" id="button_xn_k" class="btn btn-block btn-flat margin-top-10 btn-info">Xác nhận</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
            <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_lichsu" class="card collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-header alert-info">
                                <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                                    Lịch sữ kho
                                </div>
                            </div>
                            <div class="">
                                <div class="table-responsive flex-row flex-nowrap">
                                    <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center; width:5%;">#</th>
                                                <th style="text-align:center;">Người thêm</th>
                                                <th style="text-align:center;">Mã hàng</th>
                                                <th style="text-align:center;">Tổng tiền nhập</th>
                                                <th style="text-align:center;">Ghi chú</th>
                                                <th style="text-align:center;">Thời gian</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $stmt1 =  $conn->prepare("SELECT * FROM kho_nhap_vao_ls ORDER BY id DESC LIMIT 200");
                                            $stmt1->execute(array());
                                            $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                            $num = 0;
                                            foreach ($list_code as $show_fp) {
                                                $num = $num + 1;
                                                $ngay_show = date('Ymd', $show_fp['time_add']) == $time_num ? date('H:i:s', $show_fp['time_add']) : date('d-m-Y', $show_fp['time_add']);
                                                $show_fp['ghi_chu'] = $show_fp['ghi_chu'] == '' ? '...' : $show_fp['ghi_chu'];
                                                $ten_ma_hang = name_sanpham_01($show_fp['sanpham01']);
                                                $ten_nhan_vien = ucwords(sql_member($show_fp['mem'], 'name'));
                                                if ($id_level < 8) {
                                                    $show_fp['tong_tien'] = 0;
                                                }
                                                echo '<tr>
                                                    <td style="text-align:center;">' . $num . '</td>
                                                    <td style="text-align:center;">' . $ten_nhan_vien . '</td>
                                                    <td style="text-align:center;">' . $ten_ma_hang . '</td>
                                                    <td style="text-align:center;"><button type="button" id="show_popup" class="button_a" data-toggle="modal" data-target="#popup_ne" onclick="chi_tiet(\'' . $num . '\',\'' . $ten_nhan_vien . '\',\'' . $ten_ma_hang . '\',\'' . $show_fp['time_add'] . '\')">' . number_format($show_fp['tong_tien']) . 'đ</td>
                                                    <td style="text-align:center;">' . _sql($show_fp['ghi_chu']) . '</td> 
                                                    <td style="text-align:center;" title="' . date('H:i:s', $show_fp['time_add']) . '">' . $ngay_show . '</td>
                                                    </tr>';
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <?php
            }
            ?>
            <!-- <div class="col-12 mb-3">
                <div id="accordion">
                    <div id="collapse_xuatkho" class="card collapse  <?php echo $showhtml; ?>" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card" style="margin-bottom: -0.5rem;">
                            <div class="card-header alert-info d-flex justify-content-between">
                                <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                                    <div>Lịch sử xuất kho:  <?php 
                                                                if($arr_t != NULL)
                                                                 {echo $arr_t[0]. " đến ".$arr_t[1];}
                                                                 else
                                                                 {
                                                                     echo "Hôm qua đến nay";
                                                                 }
                                                                 ?>
                                    </div> 
                                    <i class="align-middle mr-2 fas fa-fw fa-search collapsed" data-toggle="collapse" data-target="#collapse_TaoDonHang_xuat" aria-expanded="false"></i>
                                </div>
                            </div>
                            <div class="">
                                <div class="table-responsive flex-row flex-nowrap">
                                    <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                        <thead>
                                            <tr>
                                                <th style="text-align:center; width:5%;">#</th>
                                                <th>Sản phẩm</th>
                                                <th style="text-align:center;">Số lượng xuất</th>
                                                <th style="text-align:center;">Hàng tồn</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            // $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_02");
                                            // $stmt1->execute(array());
                                            // $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                            // $num = 0;
                                            // $data_time =  $_SESSION['tim_kiem_ngay_xuat_kho'];
                                            // $data= get_list_code_xuat($time_php,$data_time);
                                            //     foreach ($list_code as $show_fp) 
                                            //     {
                                            //         $id_sp = $show_fp["id"];
                                            //         $tong_sp_nhap_vao = so_luong_nhap_vao_sp($id_sp);
                                            //         $tong_sp_ban_ra = so_luong_kho_ban_sp($id_sp);
                                            //         $tong_sp_ton_kho = $tong_sp_nhap_vao - $tong_sp_ban_ra;
                                            //         $so_luong = 0;
                                            //         for($i=0 ;$i< sizeof($data); $i++)
                                            //         {
                                            //             $stmt2 =  $conn->prepare("SELECT * FROM sale_sanpham_03_xuat_sum WHERE code_xuat=$data[$i] ORDER BY so_luong ASC");
                                            //             $stmt2->execute();
                                            //             $list_code2 = $stmt2->fetchALL(PDO::FETCH_ASSOC);
                                            //             foreach ($list_code2 as $show2) {
                                            //                 if($show_fp["id"] == $show2["ma_kem"])
                                            //                 {
                                            //                     $so_luong += $show2["so_luong"];
                                            //                 }
                                            //             }
                                            //         }
                                            //             $num++;
                                            //             echo '<tr>
                                            //             <td style="text-align:center;">' . $num . '</td>
                                            //             <td>'.$show_fp["ma_kem"].'</td>
                                            //             <td style="text-align:center;">'.$so_luong.'</td>
                                            //             <td style="text-align:center;">'.$tong_sp_ton_kho.'</td>
                                            //             </tr>';
                                            //         $so_luong = 0;
                                            //     }
                                            //     $num++;
                                            ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="col-12 mt-2">
                <div class="card">
                <div class="card-header alert-info d-flex justify-content-between">
                        <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size:18 px;color:white;">
                            <div>Kho hàng</div> 
                        </div>
                </div>
                    <div class="">
                        <div class="table-responsive flex-row flex-nowrap">
                            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="text-align:center; width:5%;">#</th>
                                        <th style="text-align:left;">Sản phẩm</th>
                                        <th style="text-align:center;">Nhập vào</th>
                                        <th style="text-align:center;">Bán ra</th>
                                        <th style="text-align:center;">Tồn kho</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $stmt1 =  $conn->prepare("SELECT * FROM sale_sanpham_01 ORDER BY id ASC");
                                    $stmt1->execute(array());
                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                    $num = 0;
                                    foreach ($list_code as $show_fp) {
                                        // var_dump( js052_kho_ban_ra($show_fp['id'],'so_luong')));
                                        $sl_ban  = js052_kho_ban_ra($show_fp['id'],'so_luong');
                                        $sl_nhap = js052_kho_nhap_vao_tong($show_fp['id'], 'sl');
                                        $sl_ton = $sl_nhap - $sl_ban;
                                        //  $sl_ton = sql_web($show_fp['id'], 'kho_update', 'sl');
                                        //$sl_ban  = js052_kho_ban_ra($show_fp['id'],'so_luong');
                                        // $sl_ban = $sl_nhap - $sl_ton;
                                        $num++;
                                        echo '<tr>
                                            <td style="text-align:center;">' . $num . '</td>
                                            <td style="font-family: center">' . substr($show_fp['name'],  0, 100)  . '</td> 
                                            <td style="font-family: center; text-align:center;" data-toggle="modal" data-target="#popup_ne" onclick="js_nv(' . $show_fp['id'] . ',\'' . substr($show_fp['name'],  0, 100) . '\')">' . number_format($sl_nhap) . '</td>
                                            <td style="font-family: center; text-align:center;" data-toggle="modal" data-target="#popup_ne" onclick="js_br(' . $show_fp['id'] . ',\'' . substr($show_fp['name'],  0, 100) . '\')">' . number_format($sl_ban) . '</td> 
                                            <td style="font-family: center; text-align:center;" data-toggle="modal" data-target="#popup_ne" onclick="js_tk(' . $show_fp['id'] . ',\'' . substr($show_fp['name'],  0, 100) . '\')">' . number_format($sl_ton) . '</td>
                                            </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div> 

            <script>
                function js_nv(uid, name) {
                    document.getElementById("popup_title").innerHTML = "Mã: " + uid + "-->" + name;
                    $.post("kho_hang", {
                        js_nv: uid
                    }, function(data) {
                        document.getElementById("popup_noidung").innerHTML = data;
                    });
                }

                function js_br(uid, name) {
                    document.getElementById("popup_title").innerHTML = "Mã: " + uid + "-->" + name;
                    $.post("kho_hang", {
                        js_br: uid
                    }, function(data) {
                        document.getElementById("popup_noidung").innerHTML = data;
                    });
                }

                function js_tk(uid, name) {
                    document.getElementById("popup_title").innerHTML = "Mã: " + uid + "-->" + name;
                    $.post("kho_hang", {
                        js_tk: uid
                    }, function(data) {
                        document.getElementById("popup_noidung").innerHTML = data;
                    });
                }

                function show_id_hang() {
                    var x = document.getElementById("id_hang");
                    var i = x.selectedIndex;
                    var id_hang = (x.options[i].value);
                    $.post("kho_hang", {
                        id_hang: id_hang
                    }, function(data) {
                        document.getElementById("show_du_lieu").innerHTML = data;
                    });
                }

                function chi_tiet(num, nhan_vien, ma_hang, time_add) {
                    document.getElementById("popup_title").innerHTML = "# " + num + " -->" + ma_hang + " -->" + nhan_vien;
                    $.post("kho_hang", {
                        chi_tiet_add_kho: time_add
                    }, function(data) {
                        document.getElementById("popup_noidung").innerHTML = data;
                    });
                }
                function myFunction(id_level){

                    if(id_level < 7 && id_level != 5  )
                    {
                        document.getElementById("popup_title").innerHTML = "Thông báo!";
                        document.getElementById("popup_noidung").innerHTML = "Chỉ admin mới vào được";
                        $('#popup_ne').modal('show');
                    }
                }
            </script>
            <div class="modal fade" id="popup_ne" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg_xanh_duong_nhat">
                            <h2 style="text-align:center;" id="popup_title"></h2>
                            <button type="button" class="btn btn-outline-danger" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body" id="popup_noidung"></div>
                    </div>
                </div>
            </div>
