<?php
if ($_SERVER['HTTP_HOST'] != 'api-sale.vubaodan.com') {
    header('Location: https://sale.danplay.net/');
    exit;
}

$data = explode('|', check_key($api));
$id_member = $data[0];
$id_team = $data[1];

function check_key($api)
{
    global $conn;
    $id = (int) explode('|', base64_de($api))[0];
    $key = _sql00(explode('|', base64_de($api))[1]);
    $stmt =  $conn->prepare("SELECT * FROM member WHERE id =:id");
    $stmt->execute(array(":id" => $id));
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    $name_mem = '';
    if ($member['key_string'] == $key and $member['locks'] == 0) {
        $name_mem = $member['name'];
    } else {
        echo 'error';
        exit;
    }
    return $member['id'] . '|' . $member['team'] . '|' . $name_mem;
}

function list_account($id_member, $moi, $check, $nuoi, $tot, $time_php)
{
    global $conn;
    $sql_moi = $moi == 1 ? 'status=0' : '1>2';
    $sql_tot = $tot == 1 ? 'status=5' : '1>2';
    $sql_check = $check == 1 ? 'status=9' : '1>2';
    $sql_nuoi = $nuoi == 1 ? 'status=8' : '1>2';
    $stmt1 =  $conn->prepare("SELECT * FROM facebook_account WHERE mem=$id_member AND ($sql_moi or $sql_tot or $sql_check or $sql_nuoi) ORDER BY time_use ASC LIMIT 1");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);    
    foreach ($list_code as $show) {        
        $json['id'] = $show['id'];
        $json['login'] = $show['login'];
        $json['pass'] = base64_de($show['pass']);
        $json['code2fa'] = $show['code2fa'];
        $json['cookies'] = $show['cookies'];
        $json['country'] = $show['country'];        
        $sql = "UPDATE facebook_account SET time_use='$time_php' WHERE id='" . $show['id'] . "'";
        $stmt = $conn->prepare($sql);
        $stmt->execute();
    }
    $json['proxy'] = "null";
    return json_encode($json, JSON_UNESCAPED_UNICODE);
}

function sql_account($id_member, $gia_tri, $full = 0)
{
    global $conn;
    $stmt =  $conn->prepare("SELECT * FROM auto_control_facebook WHERE id =:id");
    $stmt->execute(array(":id" => $id_member));
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($full == 0) {
        return $member[$gia_tri];
    } else {
        $json['thread_auto'] = $member['thread_auto'];
        $json['reset_ip'] = $member['reset_ip'];
        $json['limit_change_ip'] = $member['limit_change_ip'];
        $json_setup = json_decode($member['auto_setting'], true);
        foreach ($json_setup as $key => $value) {
            $json[$key] = $json_setup[$key][1];
        }
        return json_encode($json);
    }
}

function sql_update_acc($id, $mem, $key, $value)
{
    global $conn;
    $pass_new = base64_en($value);
    $sql = "UPDATE facebook_account SET $key='$pass_new' WHERE id=$id and mem=$mem";
    $stmt = $conn->prepare($sql);
    $stmt->execute();    
}