<?php
if ($id_level < 6) {
    header('Location: /');
    exit;
}
$thong_bao_key = 'Vui lòng điền đầy đủ thông tin phía dưới';
if (isset($_GET['delfp'])) {
    $idCheck = $_GET['delfp'];
    if (strlen($idCheck) <= 9) {
        header('Location: /ql_chat');
        exit;
    } else {
        $idCheck = substr($idCheck, 6);
        $idCheck = substr($idCheck, 0, -3);
        $id_key = (int) _sql01($idCheck);
        $sql = "DELETE FROM `024_chat` WHERE id=$id_key";
         $conn->exec($sql);
        header('Location: /ql_chat');
        exit;
    }
}

$status_a = (int) $status_a;
$title = 'Quản lí chat';
require 'site/widget/header.php';
?>

<main class="content">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-12 mt-2">
                <div class="card">
                    <div class="card-header alert-info">
                        <div class="navbar-collapse collapse d-flex justify-content-between mt-1 fs_18" style="float: left;font-size: px;color:white;">
                            Danh sách nội dung chat
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive flex-row flex-nowrap">
                            <table id="datatables-basic" class="table table-bordered table-striped mb-0 ellipsis " style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="text-align:center; width:5%;">#</th>
                                        <th style="text-align:center;">Member</th>
                                        <th style="text-align:center;">Nội dung</th>
                                        <th style="text-align:center;">Thời gian</th>
                                        <th style="text-align:center;">Xóa</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php

                                    $sqlAll = "SELECT COUNT(*) FROM `024_chat` ";
                                    $stmt5 = $conn->query($sqlAll);
                                    $total_records  = $stmt5->fetchColumn();
                                    $limit = $member['limit_page'] > 0 ? $member['limit_page'] : 20;
                                    $total_page = ceil($total_records / $limit);
                                    $_GET['page'] = isset($_GET['page']) ? $_GET['page'] : 0;
                                    $_GET['page'] = $_GET['page'] > 0 ? $_GET['page'] : 0;
                                    if ($total_page > 0) {
                                        $total_page_max = $total_page - 1;
                                    } else {
                                        $total_page_max = $total_page;
                                    }
                                    $_GET['page'] = $total_page_max < $_GET['page'] ? $total_page_max : $_GET['page'];
                                    $start_page = $_GET['page'] * $limit;
                                    $num_1 = 0;
                                    $stmt1 =  $conn->prepare("SELECT * FROM `024_chat`  ORDER BY id DESC LIMIT $start_page, $limit");
                                    $stmt1->execute(array());
                                    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
                                    $num = 0;

                                    foreach ($list_code as $show_fp) {
                                        $num_1 = $num_1 + 1;
                                        $num = $num_1 + $_GET['page'] * $limit;
                                        $ngay_hien_tai = date('d/m/Y', $time_php);
                                        $ngay_them = date('d/m/Y', $show_fp['time']);
                                        $ngay_show = $ngay_them == $ngay_hien_tai ? date('H:i:s', $show_fp['time']) : $ngay_them;
                                        $trang_thai = $show_fp['trang_thai'] == 0 ? "Mới thêm" : "Đang chạy";
                                        echo '<tr>
                                        <td style="text-align:center;" title="id: ' . $show_fp['id'] . '">' . $num . '</td>
                                        <td style="text-align:center;" >' . sql_member($show_fp['mem'], 'name')  . '</td>
                                        <td style="text-align:center;" title = "'.base64_html($show_fp['text']).'">' . substr(base64_html($show_fp['text']) ,  0,200)   . '</td>
                                        <td style="text-align:center;" title = "' . date('H:i:s d-m-Y ', $show_fp['time_add']) . '">' . $ngay_show . '</td>
                                        <td style="text-align:center;">';

                                        if ($id_level >= sql_member($show_fp['mem'], 'level')) {

                                            echo '<a href="/ql_chat&delfp=' . rand(100000, 999999) .  $show_fp['id'] . rand(100, 999) . '" onclick="return confirm(\'Nội dung này sẽ bị xóa? \nHệ thống không thể khôi phục\')"><i class="align-middle text_black"  data-feather="trash"></i></a>';
                                        } else {
                                            echo '...';
                                        }
                                        echo '  </td>
                                        </tr>';
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <?php
                    $i = 1;
                    for ($i; $i <= $num; $i++) {
                        echo $chuoi_popup_edit[$i];
                    }
                    load_page(_sql01($_SERVER['REQUEST_URI']), $total_page, $limit, $total_records, $total_page_max);
                    load_dialog($total_page_max, $member['id']); ?>
                </div>
            </div>
        </div>
    </div>
    </div>
    <script src="js/clipboard/dist/clipboard.min.js"></script>
    <script>
        var btns = document.querySelectorAll('button');
        var clipboard = new ClipboardJS(btns);
        clipboard.on('success', function(e) {
            console.log(e);
        });
        clipboard.on('error', function(e) {
            console.log(e);
        });

        function sllike(event) {
            // alert(event.value);
            $("#tong_tien").val(formatMoney(event.value * <?= $dongia ?>, 0) + "đ");
        }

        function formatMoney(amount, decimalCount = 2, decimal = ".", thousands = ",") {
            try {
                decimalCount = Math.abs(decimalCount);
                decimalCount = isNaN(decimalCount) ? 2 : decimalCount;

                const negativeSign = amount < 0 ? "-" : "";

                let i = parseInt(amount = Math.abs(Number(amount) || 0).toFixed(decimalCount)).toString();
                let j = (i.length > 3) ? i.length % 3 : 0;

                return negativeSign + (j ? i.substr(0, j) + thousands : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousands) + (decimalCount ? decimal + Math.abs(amount - i).toFixed(decimalCount).slice(2) : "");
            } catch (e) {
                console.log(e)
            }
        };
    </script>
    <script type="text/javascript">
    </script>