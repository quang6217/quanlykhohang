<?php
if($id_level<5){
    echo '<title>Error</title>Sorry: Chi ADMIN moi xem dc list don hang!';
    exit;
}
?><title>Xuất list đơn hàng</title>
<style>
    table,
    th,
    td {
        border: 1px solid black;
        border-collapse: collapse;
    }

    th,
    td {
        padding: 5px;
    }

    th {
        text-align: left;
    }
</style>

    <?php
    echo '<table style="width:100%">
    <tr>
        <th>#</th>
        <th>Tên KH</th>
        <th>Điện thoại</th>
        <th>Địa chỉ</th>
        <th>SL</th>
        <th>Tên SP</th>
        <th>Size - Màu</th>        
        <th>Số tiền</th>
        <th>Ghi chú</th>
        <th>Thời gian</th>
    </tr>';
    $stmt1 =  $conn->prepare("SELECT * FROM vn_don_hang_ship WHERE trang_thai=0 ORDER BY id DESC");
    $stmt1->execute();
    $list_code = $stmt1->fetchALL(PDO::FETCH_ASSOC);
    $num = 0;
    foreach ($list_code as $show) {
        $num++;
        $ngay_show = date('Ymd', $show['time_add']) == $time_num ? date('H:i:s', $show['time_add']) : date('d-m-Y', $show['time_add']);
        $mau_size = '';
        $arr_ms = explode('|',str_replace('0|','',$show['mau_sac']));
        $arr_sz = explode('|',str_replace('0|','',$show['size']));
        for($a=0; $a<=$show['sl_mua'];$a++){
            if($arr_ms[$a]>0 and $arr_sz[$a]>0){
                $mau_size = $mau_size.''.info_mau_sac($arr_ms[$a]).' - ' . info_size($arr_sz[$a]) . '<br>';
            }            
        }        
        echo '
        <tr>
        <td>' . $num . '</td>
        <td>' . $show['nameKH'] . '</td>
          <td>0' . $show['sdt'] . '</td>
          <td>' . $show['address_detail'] . ', ' . info_vn_xa_v2($show['xaKH']) . '</td>
          <td>' . $show['sl_mua'] . '</td>
          <td>' . $show['nameSP'] . '</td>
          <td>' . $mau_size . '</td>          
          <td>' . $show['total_money'] . '</td>
          <td>' . $show['ghi_chu'] . '</td>
          <td>' . $ngay_show . '</td>
        </tr>
        ';
    } 
    echo '</table>';
    ?>
