<?php
if($member['level']<8){
  header ('Location: /');
  exit;
}
$title = 'Tổng giao dịch';
require 'site/widget/header.php';
$loai_gd = isset($_GET['giao_dich'])?(int)$_GET['giao_dich']:0;
?>
<main class="content">
				<div class="container-fluid p-0">					
                <div class="row">
            <div class="col-12">
							<div class="card">
              <div class="card-header alert-info d-flex justify-content-between">
              <div class="p-2 bd-highlight" style="color:white; font-size: 18px;">Lịch sữ giao dịch</div>              
              <a class="p-2 bd-highlight" style="color:white; font-size: 18px;" data-toggle="dropdown" href="#"><i data-feather="delete"></i></a></a>
              <div class="dropdown-menu">                             
                <a class="dropdown-item" href="#" style="color:blue;">Tháng này</a>
                <a class="dropdown-item" href="#" style="color:blue;">Tháng trước</a>                       
              </div>
								</div>
                <div class="table-responsive">
								<table class="table table-bordered table-striped mb-0">

		<thead>
										<tr>
                      <th style="width:5%; text-align:center;">#</th>
                      <th style="width:10%; text-align:center;">Số tiền</th>
                      <th style="width:20%; text-align:center;">Người chuyển - số dư</th>                      
                      <th style="width:20%; text-align:center;">Người nhận - số dư</th>                                            
                      <th style="width:10%; text-align:center;">Giao dịch</th>                      
                    <th style="width:15%; text-align:center;">Ghi chú</th>
                    <th style="width:15%; text-align:center;">Thời gian</th>
										</tr>
									</thead>
									<tbody>

<?php        
        $sqlAll = "SELECT COUNT(`id`) FROM `lich_su_giao_dich`";
        if($loai_gd!=0){$sqlAll = "SELECT COUNT(`id`) FROM `lich_su_giao_dich` WHERE `loai_giao_dich`=$loai_gd";}
        $stmt5 = $conn->query($sqlAll);
        $total_records  = $stmt5->fetchColumn();
        $limit=$member['limit_page']>0?$member['limit_page']:20;
        $total_page = ceil($total_records / $limit);
        $_GET['page']=isset($_GET['page'])?$_GET['page']:0;
        $_GET['page']=$_GET['page']>0?$_GET['page']:0;
        if($total_page>0){
          $total_page_max=$total_page-1;
        } else {$total_page_max=$total_page;}
        $_GET['page']=$total_page_max<$_GET['page']?$total_page_max:$_GET['page'];
        $start_page=$_GET['page']*$limit;

        $num_1=0;
        $stmt1 =  $conn->prepare("SELECT * FROM lich_su_giao_dich ORDER BY id DESC LIMIT $start_page, $limit" );
        if($loai_gd!=0){ $stmt1 =  $conn->prepare("SELECT * FROM lich_su_giao_dich WHERE loai_giao_dich=$loai_gd ORDER BY id DESC LIMIT $start_page, $limit" );}
        $stmt1->execute(array());
        $list_code= $stmt1->fetchALL(PDO::FETCH_ASSOC);
    foreach($list_code as $show_email){
        $num_1=$num_1+1;
        $num=$num_1+$_GET['page']*$limit;      
        $du_chuyen = $show_email['so_du_chuyen'];  $du_chuyen1 = $du_chuyen>=0?'<font color=blue>'.number_format($du_chuyen,0).'đ</font>':'<font color=red>'.number_format($du_chuyen,0).'đ</font>';
        $du_nhan = $show_email['so_du_nhan'];  $du_nhan1 = $du_nhan>=0?''.number_format($du_nhan,0).'đ</font>':'<font color=red>'.number_format($du_nhan,0).'đ</font>';
        echo '<tr>
        <td style="text-align:center;">'.$num.'.</td>
        <td style="text-align:center;">'.number_format($show_email['so_tien'],0).'đ</td>
        <td style="text-align:center;"><a href="/tong_giao_dich_log&id='.$show_email['id_chuyen'].'" target="_blank">'.sql_member($show_email['id_chuyen'],'name').' --> '.$du_chuyen1.'</a></td>        
        <td style="text-align:center;"><a href="/tong_giao_dich_log&id='.$show_email['id_nhan'].'" target="_blank">'.sql_member($show_email['id_nhan'],'name').' --> '.$du_nhan1.'</a></td>        
        <td style="text-align:center;">'.sql_giao_dich($show_email['loai_giao_dich']).'</td>        
        <td style="text-align:center; '.$mau_sac.'" title="'.$show_email['ghi_chu'].'">'.substr($show_email['ghi_chu'],0,30).'</td>
        <td class="table-action"  style="text-align:center; ">'.date('H:i:s d-m-Y',$show_email['thoi_gian']).'</td>
        </tr>';

    }
?>

									</tbody>
								</table>               
								</div>
							</div>
						</div>
					</div>
                    </div>       
      
      <?php load_page($_SERVER['REQUEST_URI'],$total_page,$limit,$total_records,$total_page_max);
      load_dialog($total_page_max, $member['id']);?>
      
      </main>
      <script src="/js/clipboard/dist/clipboard.min.js"></script>

<script>
var btns = document.querySelectorAll('button');
var clipboard = new ClipboardJS(btns);

clipboard.on('success', function(e) {
console.log(e);
});

clipboard.on('error', function(e) {
console.log(e);
});</script>

<script>
    $(function() {
        $('.sidebar-toggle.d-flex.mr-2').click(function() {
            $('footer.footer').toggleClass('no-padding');
        });
    });
</script>