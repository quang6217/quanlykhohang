<?php
session_start();
define('SYSPATH', 'system/');
require SYSPATH . 'sql.php';
require SYSPATH . 'func.php';
require 'RewriteRule.php';
if ($_SERVER['REQUEST_URI'] != '/login' or $_SERVER['REQUEST_URI'] != '/login&exit=ok' or $_SERVER['REQUEST_URI'] != '/locks') {
    $_SESSION['REQUEST_URI'] = $_SERVER['REQUEST_URI'];
}
if (isset($_COOKIE['id_member'])) {
    //Tăng thêm thời gian login khi load web
    setcookie('id_member', $_COOKIE['id_member'], $time_php + 10 * 60 * 60 * 24);
    setcookie('id_pass', $_COOKIE['id_pass'], $time_php + 10 * 60 * 60 * 24);
    setcookie('token', $_COOKIE['token'], $time_php + 10 * 60 * 60 * 24);
    //Bảo mật thông tin bằng cách đăng nhập            
    $vbd_id = $_COOKIE['id_member'];
    $stmt =  $conn->prepare("SELECT * FROM member WHERE id =:id");
    $stmt->execute(array(":id" => $vbd_id));
    $member = $stmt->fetch(PDO::FETCH_ASSOC);
    $token_v2 = md5(md5(substr($member['pass'], 0, 10))) . '' . md5(md5(md5(substr($member['pass'], 10, 20))));
    if ($member['pass'] != $_COOKIE['id_pass'] or $member['pass'] == '' or $token_v2 != $_COOKIE['token']) {
        setcookie('id_member', '', $time_php - 100 * 60 * 60);
        setcookie('id_pass', '', $time_php - 100 * 60 * 60);
        session_destroy();
        header('Location: /login&exit=ok');
        exit;
    }
    if ($member['locks'] != 0) {
        require 'site/widget/locks.php';
        exit;
    }
    //Khai báo các giá trị thường xuyên sữ dụng
    $id_member = $member['id'];
    $id_team = $member['team'];
    $id_level = $member['level'];
}
if ($_GET['note'] != '') {
    if (file_exists('site/php_code_SC2V8ULyAfh5TCU/' . $url[$_GET['note']] . '.php')) {
        $head_yes = 0;
        require 'site/php_code_SC2V8ULyAfh5TCU/' . $url[$_GET['note']] . '.php';
        if ($head_yes == 99) {
            require 'site/widget/footer.php';
        }
        exit;
    }        
}
require 'site/php_code_SC2V8ULyAfh5TCU/home.php';
require 'site/widget/footer.php';
exit;
